package screens.menubar;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import screens.editor.FestivalEditor;
import screens.lineup.LineUp;
import screens.planner.PlannerView;
import controllers.FestivalController;

public class MenuActionListener implements ActionListener {
	CardLayout cardLayout;
	JPanel c;
	String currentFile;
	JFrame main;

	public MenuActionListener(final CardLayout layout, final JPanel cards,
			final JFrame main) {
		this.cardLayout = layout;
		this.c = cards;
		this.main = main;
	}

	public void actionPerformed(ActionEvent e) {

		switch (e.getActionCommand()) {
		case "Save":
			if (currentFile == null || currentFile == "") {
				currentFile = getFileLoc(true);
				FestivalController.instance.saveToFile(currentFile);
			}

			JOptionPane.showMessageDialog(null, "Festival saved.");
			break;
		case "Save as":
			currentFile = getFileLoc(true);
			FestivalController.instance.saveToFile(currentFile);
			JOptionPane.showMessageDialog(null, "Festival saved.");
			break;
		case "Load":

			currentFile = getFileLoc(false);
			FestivalController.instance.loadToFile(currentFile);
//			main.loadLayout();
			break;
		case "Exit":
			System.exit(0);
			break;
		case "Lineup":
			cardLayout.show(c, "LineUp");
			for(Component com :  c.getComponents()) {
				if(com instanceof LineUp) {
					LineUp lineup = (LineUp)com;
					lineup.getMainContent().refreshArtists();
					break;
				}
			}
			break;
		case "Agenda":
			cardLayout.show(c, "TableView");
			break;
		case "Editor":
			cardLayout.show(c, "FestivalEditor");
			for(Component com :  c.getComponents()) {
				if(com instanceof FestivalEditor) {
					FestivalEditor editor = (FestivalEditor)com;
					editor.reload();
					break;
				}
			}
			break;
		case "Planner":
			cardLayout.show(c, "PlannerView");
			for(Component com :  c.getComponents()) {
				if(com instanceof PlannerView) {
					PlannerView planner = (PlannerView)com;
					planner.refreshEvents();
					break;
				}
			}
			break;
		}

	}

	public String getFileLoc(boolean save) {
		JFileChooser chooser = new JFileChooser();
		FileFilter filter = new FileNameExtensionFilter("festival", "festival");
		chooser.addChoosableFileFilter(filter);
		chooser.setAcceptAllFileFilterUsed(false);
		if (save) {
			chooser.setFileSelectionMode(JFileChooser.SAVE_DIALOG);
		} else {
			chooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
		}
		int option = chooser.showOpenDialog(null);
		if (option == JFileChooser.APPROVE_OPTION) {
			File sf = chooser.getSelectedFile();
			String path = sf.getPath();
			if (!path.endsWith(".festival"))
				return path + ".festival";
			return path;
		}

		return "";

	}
}
