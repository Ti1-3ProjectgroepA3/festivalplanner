package screens.menubar;

import java.awt.CardLayout;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class MyMenuBar extends JMenuBar{
	public MyMenuBar(final CardLayout layout, final JPanel cards, final JFrame main){
		
		//initialize menu components
		JMenu fileMenu = new JMenu("File");
		JMenu view = new JMenu("View");
		//file menu components
		JMenuItem newMenuItem = new JMenuItem("Save", KeyEvent.VK_N);
		fileMenu.add(newMenuItem);

		JMenuItem saveItem = new JMenuItem("Save as", KeyEvent.VK_S);
		fileMenu.add(saveItem);

		JMenuItem loadItem = new JMenuItem("Load",KeyEvent.VK_L);
		fileMenu.add(loadItem);

		JMenuItem exitItem = new JMenuItem("Exit",KeyEvent.VK_F4);
		fileMenu.add(exitItem);

		//view menu components		
		JMenuItem lineUp = new JMenuItem("Lineup", KeyEvent.VK_L);
		view.add(lineUp);

		JMenuItem agenda = new JMenuItem("Agenda", KeyEvent.VK_A);
		view.add(agenda); 

		JMenuItem simulator = new JMenuItem("Editor", KeyEvent.VK_I);
		view.add(simulator);
		
		JMenuItem planner = new JMenuItem("Planner", KeyEvent.VK_I);
		view.add(planner);
		
		
		//set shortcuts
		fileMenu.setMnemonic(KeyEvent.VK_F);
		view.setMnemonic(KeyEvent.VK_V);
	
		MenuActionListener menuActionListener = new MenuActionListener(layout,cards, main);
		
		//actionlisteners
		newMenuItem.addActionListener(menuActionListener);
		
		saveItem.addActionListener(menuActionListener);
		loadItem.addActionListener(menuActionListener);
		exitItem.addActionListener(menuActionListener);
		lineUp.addActionListener(menuActionListener);
		agenda.addActionListener(menuActionListener);
		simulator.addActionListener(menuActionListener);
		planner.addActionListener(menuActionListener);
		//add components to MenuBar
		this.add(fileMenu);
		this.add(view);

	}
}
