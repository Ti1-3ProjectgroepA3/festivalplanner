package screens.tableview.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class MyTable extends JTable{
	public MyTable(){
		//customize table header
		getTableHeader().setBackground(new Color(18,19,20));
		getTableHeader().setForeground(new Color(240,240,240));
		getTableHeader().setBorder(BorderFactory.createEmptyBorder());
		getTableHeader().setPreferredSize(new Dimension(getPreferredSize().width, 35));
		
		//customize cell ui
		setSelectionBackground(new Color(34,35,38));
		setSelectionForeground(new Color(240,240,240));
		setForeground(new Color(150,150,150));
		setSelectionModel(new ForcedListSelectionModel());
		setShowVerticalLines(false);
		setIntercellSpacing(new Dimension(0, 1));
		setBorder(null);
		setBackground(new Color(18, 19, 20));
		setRowHeight(40);
		setModel(new MyTableModel());
	}
}
