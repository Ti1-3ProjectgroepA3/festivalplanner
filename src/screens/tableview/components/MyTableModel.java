package screens.tableview.components;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.table.AbstractTableModel;

import models.Artist;
import models.Event;
import controllers.FestivalController;

public class MyTableModel extends AbstractTableModel {
	private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	private static final String[] HEADER = new String[] { "Artist",
			"Popularity", "Stagename", "Begin time", "End time" };

	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return HEADER[column];
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return FestivalController.instance.getFestivalPlanner().getEvents()
				.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return HEADER.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		ArrayList<Event> events = FestivalController.instance
				.getFestivalPlanner().getEvents();
		Event event = events.get(rowIndex);

		Calendar eStartTime = event.getStartTime();
		Calendar eEndTime = event.getEndTime();
		Date date1 =  eStartTime.getTime();
		Date date2 = eEndTime.getTime();
		String startTime = dateFormat.format(date1);
		String endTime = dateFormat.format(date2);	
		
		switch (columnIndex) {
		case 0:
			String string = "";
			for (Artist a : event.getArtists()) {
				string += a.getName() + " / ";

			}
			return string.substring(0, string.length() - 3);

		case 1:
			return event.getPopularity();
		case 2:
			if (event.getStage() == null)
				return "Geen stage";
			else
				return event.getStage().getName();
		case 3:
			return startTime;
		case 4:
			return endTime;
		default:
			return "Undefined";
		}

	}

}
