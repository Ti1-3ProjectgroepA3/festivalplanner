package screens.tableview.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class MyTableSorter extends JPanel{
	public MyTableSorter(MyTable table){
		
		setBackground(new Color (18,19,21));
		setLayout(new FlowLayout(FlowLayout.TRAILING));
		
		TableModel model = table.getModel();
		
		final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
	    table.setRowSorter(sorter);
		JTextField filterText = new JTextField(15);
		
		 JButton button = new JButton("Filter");
		 button.setPreferredSize(new Dimension(200,30));
		 button.setBackground(new Color(45,48,51));
		 button.setForeground(new Color(240,240,240));
		 button.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		 button.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		      String text = filterText.getText();
		      if (text.length() == 0) {
		        sorter.setRowFilter(null);
		      } else {
		        sorter.setRowFilter(RowFilter.regexFilter(text));
		      }
		    }
		 });
		
		table.setFillsViewportHeight(true);
		
		add(filterText);
		add(button);
		
	}
}
