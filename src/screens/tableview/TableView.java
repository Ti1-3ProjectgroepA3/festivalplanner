package screens.tableview;
import java.awt.event.*;
import java.awt.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import screens.tableview.components.MyTable;
import screens.tableview.components.MyTableSorter;

public class TableView extends JPanel {
	public TableView(){
		setLayout(new BorderLayout());
		
		MyTable table = new MyTable();
		MyTableSorter searchPane = new MyTableSorter(table);
		
		add(table,BorderLayout.CENTER);
		add(searchPane,BorderLayout.SOUTH);
		JScrollPane scrollpane = new JScrollPane(table);
		scrollpane.setForeground(new Color(240,240,240));
		add(scrollpane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(18,19,21));
		scrollpane.setCorner(JScrollPane.UPPER_RIGHT_CORNER, panel);
	}
	
}
	
	