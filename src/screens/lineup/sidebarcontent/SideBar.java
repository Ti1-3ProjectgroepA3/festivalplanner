package screens.lineup.sidebarcontent;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import models.Artist;
import screens.lineup.sidebarcontent.outlookbar.JOutlookBar;
import screens.lineup.sidebarcontent.topinfo.TopInfoBar;
import screens.lineup.sidebarcontent.topinfo.TopInfoLineUp;

public class SideBar extends JPanel {

	TopInfoLineUp topDiv;
	JOutlookBar artistInfo;
	TopInfoBar bar;
	Artist artist;
	String name;
	String desc;
	String img;

	public SideBar(Artist a, JOutlookBar outlook, TopInfoBar infoBar,
			TopInfoLineUp picture) {
		// set fields
		artist = a;
		name = artist.getName();
		img = artist.getImg();
		desc = artist.getDescription();

		// create
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		topDiv = picture;
		bar = infoBar;
		artistInfo = outlook;

		bar.setBackground(new Color(18,19,20));
		artistInfo.artistPanel.setBackground(new Color(34,35,38));
		artistInfo.schemaPanel.setBackground(new Color(34,35,38));
		
		add(topDiv);
		add(bar);
		add(artistInfo);
	}

	public void setArtistInfo(String name, String desc) {
		artistInfo.setDesc(desc);
		artistInfo.repaint();
	}

	public void setTopDiv(String img) {
		topDiv.setImage(topDiv.loadImage(img));
	}

	public void setSchema(Artist a){
		artistInfo.fillEventList(a);
	}
	
	public void setBar(String name) {
		bar.setBar(name);
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
