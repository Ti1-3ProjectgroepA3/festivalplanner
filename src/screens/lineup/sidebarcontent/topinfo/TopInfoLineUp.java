package screens.lineup.sidebarcontent.topinfo;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.imgscalr.Scalr;

public class TopInfoLineUp extends JPanel {

	BufferedImage image;
	Scalr.Mode mode;
	public TopInfoLineUp(String img) {
		setPreferredSize(new Dimension(250,250));
		image = loadImage(img);
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		if(image == null)
			return;
		mode = this.getScale();
		// achtergrond
		BufferedImage scaledBgImage = Scalr.resize(image, mode, 250,
				Scalr.OP_ANTIALIAS);
		g2d.drawImage(scaledBgImage, (getWidth() / 2)
				- (scaledBgImage.getWidth() / 2), (getHeight() / 2)
				- (scaledBgImage.getHeight() / 2), null);

//		// darken
//		g2d.setPaint(new Color(34, 35, 38, 45));
//		Shape shape = new Rectangle(0, 0, getWidth(), getHeight());
//		g2d.fill(shape);
//		g2d.draw(shape);
//
//		// pictogram
//		BufferedImage scaledImage = Scalr.resize(image, mode, 120,
//				Scalr.OP_ANTIALIAS);
//		BufferedImage circleImage = makeCircle(scaledImage);
//		g2d.drawImage(circleImage, (getWidth() / 2)
//				- (circleImage.getWidth() / 2), (getHeight() / 2)
//				- (circleImage.getHeight() / 2), null);

//		g2d.dispose();
	}
	

	public BufferedImage loadImage(String fileName) {

		BufferedImage img = null;

		try {
			img = ImageIO.read(new File(fileName));
		} catch (Exception e) {
			try {
				img = ImageIO.read(new File("images/default_artist.jpg"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return img;
	}

	public Scalr.Mode getScale(){
		if (image.getHeight() > image.getWidth()) {
			mode = Scalr.Mode.FIT_TO_WIDTH;
		} else {
			mode = Scalr.Mode.FIT_TO_HEIGHT;
		}
		return mode;
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
		repaint();
	}
}
