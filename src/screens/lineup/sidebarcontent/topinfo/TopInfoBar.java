package screens.lineup.sidebarcontent.topinfo;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class TopInfoBar extends JPanel{
	JLabel artiest;
	public TopInfoBar(String name){
		artiest = new JLabel();
		artiest.setEnabled(true);
		artiest.setForeground(new Color(240,240,240));
		artiest.setText("Artiest:  "+ name);
		artiest.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
		artiest.setPreferredSize(new Dimension(250,32));
		add(artiest);
	}
	
	public void setBar(String name){
		artiest.setText("Artiest:  "+ name);
		repaint();
	}
}
