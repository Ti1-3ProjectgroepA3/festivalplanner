package screens.lineup;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import models.Artist;
import screens.lineup.maincontent.MainContentLineUp;
import screens.lineup.sidebarcontent.SideBar;
import screens.lineup.sidebarcontent.outlookbar.JOutlookBar;
import screens.lineup.sidebarcontent.topinfo.TopInfoBar;
import screens.lineup.sidebarcontent.topinfo.TopInfoLineUp;
import controllers.FestivalController;
import designManagers.customComponents.MyScrollbarUI;

@SuppressWarnings("serial")
public class LineUp extends JPanel {

	private SideBar sideBar;
	private MainContentLineUp mainContent;
	private JScrollPane mainContainer;
	private ArrayList<Artist> artists;
	private JOutlookBar outlook;
	
	public LineUp(FestivalController festivalController) {
		// create fields
		artists = festivalController.getFestivalPlanner().getArtists();
		// set layout
		setLayout(new BorderLayout());
		if(artists.isEmpty()){
			artists.add(new Artist("no_artist", "default_artist", "no artist"));
		}
		JOutlookBar outlook = new JOutlookBar(artists.get(0).getName(), artists.get(0).getDescription());
		TopInfoBar bar = new TopInfoBar(artists.get(0).getName());
		TopInfoLineUp picture = new TopInfoLineUp(artists.get(0).getImg());
		// add framework
		sideBar = new SideBar(artists.get(0),outlook,bar,picture);

		mainContent = new MainContentLineUp(artists,this);
		mainContainer = new JScrollPane(mainContent);
		mainContainer.setViewportView(mainContent);
		mainContainer.setBorder(null);
		mainContainer.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		mainContainer.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		add(mainContainer, BorderLayout.CENTER);
		add(sideBar, BorderLayout.EAST);
		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				int screenWidth = getWidth();
				int screenHeight = getHeight();
				setPreferredSize(new Dimension(screenWidth, screenHeight));
				sideBar.setPreferredSize(new Dimension(250,
						screenHeight));
				mainContainer.setPreferredSize(new Dimension( screenWidth - 250,
						screenHeight));
			}
		});
		
	}

	public SideBar getSideBar() {
		return sideBar;
	}
	public MainContentLineUp getMainContent(){
		return mainContent;
	}

}
