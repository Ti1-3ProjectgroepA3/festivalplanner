package screens.lineup.maincontent;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

import controllers.FestivalController;
import designManagers.WrapLayout;
import models.Artist;
import models.FestivalPlanner;
import screens.lineup.LineUp;

@SuppressWarnings("serial")
public class MainContentLineUp extends JPanel {

	ArrayList<Artist> artists;
	ArrayList<ArtistListItem> items = new ArrayList<ArtistListItem>();
	LineUp lineup;
	WrapLayout layout;
	FestivalPlanner planner;
	int size;
	public MainContentLineUp(ArrayList<Artist> a, LineUp lineup) {
		this.setBackground(new Color(18,19,20));
		// lineup
		this.lineup = lineup;
		artists = a;
		planner = FestivalController.instance.getFestivalPlanner();
		// create
		setName("main");
		setLayout(layout = new WrapLayout(WrapLayout.LEFT, 10, 10));
		setSize(new Dimension(400, 1100));
		
		//fields
		int colAmount = 3;
		int size = (getWidth() / colAmount) - ((10 / 3) * 2);
		this.size = size;
		setSize(new Dimension(getWidth(),	getHeight()));
		refreshArtists();
		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				setSize(new Dimension(getWidth(), getHeight()));
			}
		});

	}
	
	public void refreshArtists(){
		removeAll();
		for (Artist artist : planner.getArtists()) {
			add(new ArtistListItem(size, artist, lineup));
		}
		revalidate();
	}
	
}
