package screens.lineup.maincontent;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import models.Artist;

import org.imgscalr.Scalr;

import screens.lineup.LineUp;
import screens.lineup.sidebarcontent.SideBar;

@SuppressWarnings("serial")
public class ArtistListItem extends JPanel {
	BufferedImage image;
	String name;
	String bandImg;
	String desc;
	Integer bgSize;
	LineUp lineup;
	Scalr.Mode mode;
	
	public ArtistListItem(int size, Artist a, LineUp lineup) {
		setLayout(null);
		this.lineup = lineup;
		name = a.getName();
		bandImg = a.getImg();
		desc = a.getDescription();
		bgSize = size;
		setPreferredSize(new Dimension(bgSize, bgSize));
		image = loadImage(bandImg);
		mode = this.getScale();
		
//		System.out.println("name: " + name + "\nimg: " + bandImg);
		this.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				SideBar sidebar = lineup.getSideBar();
				sidebar.setBar(name);
				sidebar.setArtistInfo(name, desc);
				sidebar.setTopDiv(bandImg);
				sidebar.setSchema(a);
				// syso
//				System.out.println("name: " + lineup.getSideBar().getName()
//						+ "\nimg: " + lineup.getSideBar().getImg());

				repaint();
			}

		});
	}

	public void setBgSize(Integer bgSize) {
		this.bgSize = bgSize;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		// pictogram
		BufferedImage scaledImage = Scalr.resize(image, mode, bgSize);
		g2d.drawImage(scaledImage, (getWidth() / 2)
				- (scaledImage.getWidth() / 2), (getHeight() / 2)
				- (scaledImage.getHeight() / 2), null);

		g2d.setPaint(new Color(34, 35, 38));
		Shape shape = new Rectangle(0, getHeight() - 28, getWidth(), 28);
		g2d.fill(shape);
		g2d.draw(shape);

		g2d.setPaint(new Color(241, 241, 241));
		g2d.drawString(name, 10, getHeight() - 10);

	}

	BufferedImage loadImage(String fileName) {

		BufferedImage img = null;

		try {
			img = ImageIO.read(new File(fileName));
		} catch (Exception e) {
			try {
				img = ImageIO.read(getClass().getResourceAsStream(
						"/default_artist.jpg"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return img;
	}
	public Scalr.Mode getScale(){
		if (image.getHeight() > image.getWidth()) {
			mode = Scalr.Mode.FIT_TO_WIDTH;
		} else {
			mode = Scalr.Mode.FIT_TO_HEIGHT;
		}
		return mode;
	}
}