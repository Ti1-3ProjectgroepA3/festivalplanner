package screens;

import java.awt.CardLayout;

import javax.swing.JPanel;

import controllers.FestivalController;
import screens.lineup.LineUp;
import screens.tableview.TableView;

public class ScreenManager extends JPanel{
	CardLayout cardLayout = new CardLayout();
	
	JPanel lineup;
	JPanel table;
	public ScreenManager(){
		setLayout(cardLayout);
		
		table = new TableView();
		lineup = new LineUp(FestivalController.instance);
		
		this.add(lineup, "LineUp");
		this.add(table, "TableView");
	}
	public CardLayout getCardLayout() {
		return cardLayout;
	}
	public JPanel getLineup() {
		return lineup;
	}
	public JPanel getTable() {
		return table;
	}
}
