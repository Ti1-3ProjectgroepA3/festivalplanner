package screens.editor.listpanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EmptyBorder;

import models.Artist;
import models.Event;
import models.FestivalPlanner;
import models.Stage;
import controllers.FestivalController;

public class EventEditor extends JPanel {
	FestivalPlanner data = FestivalController.instance.getFestivalPlanner();
	SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	DefaultListModel defaultList = new DefaultListModel();
	JList eventLijst = new JList(defaultList);

	public EventEditor() {
		setLayout(new BorderLayout());
		JScrollPane scrollpane = new JScrollPane(eventLijst);
		// add(scrollpane, BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel(new GridLayout(0, 1));
		JPanel editPanel = new JPanel(new GridLayout(0, 1));
		editPanel.add(scrollpane, BorderLayout.NORTH);
		JButton add = new JButton("Add");

		// voegt item toe aan lijst
		buttonPanel.add(add);
		add.addActionListener(new ActionListener() {
			// opent venster om gegevens artiest in te vullen
			public void actionPerformed(ActionEvent e) {
				EventEditorPopup popup = new EventEditorPopup(null,
						EventEditor.this);

			}
		});

		// verwijdert een item van de lijst
		JButton remove = new JButton("Remove");
		buttonPanel.add(remove);
		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Event event = null;
				if (eventLijst.isSelectionEmpty()) {

					JOptionPane.showMessageDialog(null,
							"Geen event geselecteerd");
					return;

				}
				if (JOptionPane.showConfirmDialog(null,
						"Weet je zeker dat je de event wilt verwijderen?", "",
						JOptionPane.YES_NO_OPTION) == 0) {
					event = (Event) eventLijst.getSelectedValue();

					FestivalController.instance.getFestivalPlanner()
							.getEvents().remove(event);
					updateItems();
				}

			}
		});

		// wijzigt een event in de lijst
		JButton edit = new JButton("Edit");
		buttonPanel.add(edit);
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Event event = null;
				if (eventLijst.isSelectionEmpty()) {

					JOptionPane.showMessageDialog(null,
							"Geen event geselecteerd");
					return;

				}
				event = (Event) eventLijst.getSelectedValue();
				EventEditorPopup popup = new EventEditorPopup(event,
						EventEditor.this);
				updateItems();

			}
		});

		add(editPanel);
		add(buttonPanel, BorderLayout.SOUTH);
		updateItems();
	}

	public void updateItems() {
		defaultList.clear();
		for (Event e : FestivalController.instance.getFestivalPlanner()
				.getEvents()) {

			defaultList.addElement(e);
		}
	}

	public class EventEditorPopup extends JDialog {
		JSpinner startTimeInput = new JSpinner(new SpinnerDateModel());
		JSpinner endTimeInput = new JSpinner(new SpinnerDateModel());
		JSpinner popularityInput = new JSpinner();

		JComboBox<Stage> stageInput = new JComboBox<Stage>(
				FestivalController.instance.getFestivalPlanner().getStages()
						.toArray(new Stage[0]));
		JTextField artistsInput = new JTextField("Artiesten");

		JPanel panelArtistSelect = new JPanel(new GridLayout(1, 3));

		JList<Artist> availableArtists = new JList<Artist>(
				new DefaultListModel<Artist>());
		JList<Artist> selectedArtists = new JList<Artist>(
				new DefaultListModel<Artist>());
		Event event;
		public EventEditorPopup(Event e1, EventEditor eventEditor) {
			this.event = e1;
			setPreferredSize(new Dimension(300, 300));
			((JPanel) this.getContentPane()).setBorder(new EmptyBorder(10, 10,
					10, 10));

			JPanel info = new JPanel(new GridLayout(4, 2, 10, 0));
			JLabel label = null;
			startTimeInput.setEditor(new JSpinner.DateEditor(startTimeInput,
					"HH:mm:ss"));
			endTimeInput.setEditor(new JSpinner.DateEditor(endTimeInput,
					"HH:mm:ss"));

			info.add(label = new JLabel("Start tijd:"));
			label.setBorder(new EmptyBorder(0, 0, 0, 20));
			info.add(startTimeInput);
			info.add(label = new JLabel("Eind tijd:"));
			label.setBorder(new EmptyBorder(0, 0, 0, 20));
			info.add(endTimeInput);
			try {
				SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss");
				if (event == null) {
					startTimeInput.setValue(formater.parse("08:00:00"));
					endTimeInput.setValue(formater.parse("09:00:00"));

				} else {
					startTimeInput.setValue(event.getStartTime().getTime());
					endTimeInput.setValue(event.getEndTime().getTime());
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			info.add(label = new JLabel("Popularitijd:"));
			label.setBorder(new EmptyBorder(0, 0, 0, 20));
			info.add(popularityInput);
			if (event != null)
				popularityInput.setValue(event.getPopularity());
			info.add(label = new JLabel("Stage:"));
			label.setBorder(new EmptyBorder(0, 0, 0, 20));
			info.add(stageInput);
			if (event != null && event.getStage() != null)
				stageInput.setSelectedItem(event.getStage());

			this.add(info, BorderLayout.NORTH);

			for (Artist a : FestivalController.instance.getFestivalPlanner()
					.getArtists()) {
				if (event != null && event.getArtists().contains(a)) {
					((DefaultListModel<Artist>) selectedArtists.getModel())
							.addElement(a);
				} else {
					((DefaultListModel<Artist>) availableArtists.getModel())
							.addElement(a);

				}
			}

			JPanel panelArtistSelectButtons = new JPanel(new GridLayout(2, 1));
			panelArtistSelectButtons.setBorder(new EmptyBorder(0, 20, 0, 20));
			JButton buttonTmp = null;
			panelArtistSelectButtons.add(buttonTmp = new JButton(">>"));
			buttonTmp.addActionListener(new ActionListener() { // Naar event toe
						public void actionPerformed(ActionEvent e) {
							if (availableArtists.isSelectionEmpty())
								return;
							Artist a = availableArtists.getSelectedValue();
							((DefaultListModel<Artist>) availableArtists
									.getModel()).removeElement(a);
							((DefaultListModel<Artist>) selectedArtists
									.getModel()).addElement(a);
						}
					});
			panelArtistSelectButtons.add(new JButton("<<"));
			buttonTmp.addActionListener(new ActionListener() { // Van event toe
						public void actionPerformed(ActionEvent e) {
							if (selectedArtists.isSelectionEmpty())
								return;
							Artist a = selectedArtists.getSelectedValue();
							((DefaultListModel<Artist>) availableArtists
									.getModel()).addElement(a);
							((DefaultListModel<Artist>) selectedArtists
									.getModel()).removeElement(a);
						}
					});

			panelArtistSelect.add(availableArtists);
			panelArtistSelect.add(panelArtistSelectButtons);
			panelArtistSelect.add(selectedArtists);

			this.add(panelArtistSelect, BorderLayout.CENTER);

			JPanel buttons = new JPanel(new GridLayout(1, 0));
			JButton ok = new JButton("OK");
			buttons.add(ok);
			ok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ArrayList<Artist> artists = new ArrayList<Artist>();
					DefaultListModel<Artist> defaultListModelArtists = ((DefaultListModel<Artist>) selectedArtists
							.getModel());
					for (int i = 0; i < defaultListModelArtists.getSize(); i++) {
						artists.add(defaultListModelArtists.getElementAt(i));
					}
					Calendar calEnd = Calendar.getInstance();
					Calendar calStart = Calendar.getInstance();
					calStart.setTime(((Date) startTimeInput.getValue()));
					calEnd.setTime(((Date) endTimeInput.getValue()));

					Stage s = null;
					if (stageInput.getSelectedItem() instanceof Stage)
						s = (Stage) stageInput.getSelectedItem();

					if (event == null) {
						event = new Event(calStart, calEnd,
								(int) popularityInput.getValue(), artists);

						if (FestivalController.instance.getFestivalPlanner()
								.checkEventExistWithTime(event, calStart,
										calEnd)) {
							JOptionPane
									.showMessageDialog(null,
											"Event mag niet overlappen op dezelfde stage.");
							return;
						}
					} else {

						if (FestivalController.instance.getFestivalPlanner()
								.checkEventExistWithTime(event, calStart,
										calEnd)) {
							JOptionPane
									.showMessageDialog(null,
											"Event mag niet overlappen op dezelfde stage.");
							return;
						}
						event.getArtists().clear();
						event.getArtists().addAll(artists);
						event.setStartTime(calStart);
						event.setEndTime(calEnd);
					}
					if (stageInput.getSelectedItem() instanceof Stage)
						event.setStage((Stage) stageInput.getSelectedItem());
					if (!FestivalController.instance.getFestivalPlanner()
							.getEvents().contains(event))
						FestivalController.instance.getFestivalPlanner()
								.addEvent(event);

						event.setPopularity((int) popularityInput.getValue());
					EventEditorPopup.this.dispose();
					updateItems();

				}
			});
			JButton cancel = new JButton("Annuleren");
			cancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					EventEditorPopup.this.dispose();
				}
			});
			buttons.add(cancel);

			buttons.setBorder(new EmptyBorder(10, 0, 10, 0));
			this.add(buttons, BorderLayout.SOUTH);

			pack();
			setVisible(true);

		}
	}
}
