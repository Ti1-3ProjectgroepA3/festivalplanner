package screens.editor.listpanels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import models.Stage;
import controllers.FestivalController;

public class StageEditor extends JPanel {
	DefaultListModel defaultList;
	JList artiestenLijst;

	public StageEditor() {
		setLayout(new BorderLayout());
		defaultList = new DefaultListModel();
		artiestenLijst = new JList(defaultList);
		JScrollPane scrollpane = new JScrollPane(artiestenLijst);
		updateItems();
		add(scrollpane, BorderLayout.CENTER);

		JPanel buttonPanel = new JPanel(new GridLayout(0, 1));

		JButton add = new JButton("Add");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StageEditorPopup popup = new StageEditorPopup(null);
			}
		});
		buttonPanel.add(add);
		JButton remove = new JButton("Remove");
		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Stage selectedStage = getSelectedStage();
				if (selectedStage == null) {
					JOptionPane.showMessageDialog(null,
							"Geen stage geselecteerd");
					return;
				}
				if (JOptionPane.showConfirmDialog(null,
						"Weet je zeker dat je de stage wilt verwijderen?", "",
						JOptionPane.YES_NO_OPTION) == 0) {
					FestivalController.instance.getFestivalPlanner()
							.getStages().remove(selectedStage);
				}
				updateItems();
			}
			
		});
		buttonPanel.add(remove);
		JButton edit = new JButton("Edit");
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Stage selectedStage = getSelectedStage();
				if (selectedStage == null) {
					JOptionPane.showMessageDialog(null,
							"Geen stage geseleceerd");
					return;
				}
				StageEditorPopup popup = new StageEditorPopup(selectedStage);
			}
		});
		buttonPanel.add(edit);
		add(buttonPanel, BorderLayout.SOUTH);
		// artiestenLijst.setBorder(BorderFactory.createMatteBorder(-1, 1, , -1,
		// Color.black));
	}

	private Stage getSelectedStage() {
		if(artiestenLijst.isSelectionEmpty())
			return null;
		Object obj = defaultList.get(artiestenLijst.getSelectedIndex());
		if (obj instanceof Stage)
			return (Stage) obj;
		return null;
	}

	public void updateItems() {
		defaultList.clear();
		int counter = 0;
		for (Stage s : FestivalController.instance.getFestivalPlanner()
				.getStages()) {
			defaultList.add(counter, s);
			counter++;
		}
	}

	private class StageEditorPopup extends JDialog {
		private Stage stage;

		public StageEditorPopup(Stage stage) {

			setTitle("Add stage");
			setSize(300, 300);
			((JPanel) this.getContentPane()).setBorder(new EmptyBorder(10, 10,
					10, 10));

			JPanel info = new JPanel(new GridLayout(2, 2));

			JTextField name = new JTextField("");
			JSpinner maxVisitorsInput = new JSpinner();
			JLabel label;

			maxVisitorsInput.setValue(1000);

			info.add(label = new JLabel("Naam:"));
			label.setBorder(new EmptyBorder(0, 0, 0, 20));
			info.add(name);
			info.add(label = new JLabel("Maximaal bezoekers:"));
			label.setBorder(new EmptyBorder(0, 0, 0, 20));
			info.add(maxVisitorsInput);
			this.add(info, BorderLayout.NORTH);

			JPanel buttons = new JPanel(new GridLayout(1, 0));
			JButton ok = new JButton("OK");
			buttons.add(ok);
			ok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						int maxVisitors = Integer.parseInt(maxVisitorsInput
								.getValue().toString());
						if (stage != null) {
							stage.setName(name.getText());
							stage.setMaxVisitors(maxVisitors);

						} else {
							FestivalController.instance
									.getFestivalPlanner()
									.getStages()
									.add(new Stage(name.getText(), maxVisitors));
						}
						StageEditorPopup.this.dispose();
						updateItems();
					} catch (java.lang.NumberFormatException ex) {
						JOptionPane.showMessageDialog(null,
								"Maximaal bezoekers moet een nummer zijn.");
					}
				}
			});
			JButton cancel = new JButton("Annuleren");
			cancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					StageEditorPopup.this.dispose();
				}
			});
			buttons.add(cancel);

			buttons.setBorder(new EmptyBorder(10, 0, 10, 0));
			this.add(buttons, BorderLayout.SOUTH);
			pack();
			setVisible(true);

		}
	}

}
