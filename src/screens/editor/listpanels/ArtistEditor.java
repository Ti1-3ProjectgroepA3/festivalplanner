package screens.editor.listpanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import models.Artist;
import models.FestivalPlanner;
import controllers.FestivalController;

public class ArtistEditor extends JPanel {
	FestivalPlanner data = FestivalController.instance.getFestivalPlanner();
	JList artiestenLijst;
	DefaultListModel<Artist> defaultList = new DefaultListModel();

	public ArtistEditor() {
		setLayout(new BorderLayout());
		artiestenLijst = new JList<>(defaultList); 
		JScrollPane scrollpane = new JScrollPane(artiestenLijst);
//		add(scrollpane, BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel(new GridLayout(0, 1));
		JPanel editPanel = new JPanel(new GridLayout(0, 1));
		editPanel.add(scrollpane, BorderLayout.NORTH);
		JButton add = new JButton("Add");

		// voegt item toe aan lijst
		buttonPanel.add(add);
		add.addActionListener(new ActionListener() {
			// opent venster om gegevens artiest in te vullen
			String imgLocation = null;

			public void actionPerformed(ActionEvent e) {
				JDialog addArtist = new ArtistEditorDialog(null,
						ArtistEditor.this);
			}
		});

		// verwijdert een item van de lijst
		JButton remove = new JButton("Remove");
		buttonPanel.add(remove);
		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Artist a = getSelectedArtist();        
                if (a == null) {
					JOptionPane.showMessageDialog(null,
							"Geen artist geselecteerd");
					return;
				}

                if (JOptionPane.showConfirmDialog(null,
						"Weet je zeker dat je de artiest wilt verwijderen?", "",
						JOptionPane.YES_NO_OPTION) == 0) {
					FestivalController.instance.getFestivalPlanner()
							.getArtists().remove(a);
				}
				updateItems();
				
			}
		});

		// wijzigt een item in de lijst

		JButton edit = new JButton("Edit");
		buttonPanel.add(edit);
        edit.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                Artist a = getSelectedArtist();        
                if (a == null) {
					JOptionPane.showMessageDialog(null,
							"Geen stage geselecteerd");
					return;
				}
				
				JDialog addArtist = new ArtistEditorDialog(a,
						ArtistEditor.this);
			}
		});

		add(editPanel);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	public Artist getSelectedArtist() {
		if (artiestenLijst.isSelectionEmpty())
			return null;
		if (artiestenLijst.getSelectedValue() instanceof Artist)
			return (Artist) artiestenLijst.getSelectedValue();
		return null;
	}
	public void updateItems() {
		defaultList.clear();
		for(Artist a : FestivalController.instance.getFestivalPlanner()
				.getArtists()) {
			defaultList.addElement(a);
		}
	}
	

	private class ArtistEditorDialog extends JDialog {
		JLabel picLabel = new JLabel("Geen afbeelding");
		String image;

		public ArtistEditorDialog(Artist a, ArtistEditor artistEditor) {
			if (a == null) {
				setTitle("Add artist");
			} else {
				BufferedImage pic;
				try {
					pic = ImageIO.read(new File(a.getImg()));
					picLabel.setText("");
					picLabel.setIcon(new ImageIcon(pic));
					image = a.getImg();
				} catch (Exception e1) {
					picLabel.setText("Geen afbeelding");
					picLabel.setIcon(null);
					pic = null;
					// TODO Auto-generated catch block
//					e1.printStackTrace();
				}
				setTitle("Edit artist");
			}

			setPreferredSize(new Dimension(450, 400));
			((JPanel) this.getContentPane()).setBorder(new EmptyBorder(10, 10,
					10, 10));
			JPanel info = new JPanel(new GridLayout(2, 2));

			JTextField name = new JTextField("");
			JButton selectImage = new JButton("Selecteer afbeelding");
			selectImage.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JFileChooser chooser = new JFileChooser();
					FileFilter filter = new FileNameExtensionFilter(
							"JPEG bestand", "jpg", "jpeg");
					chooser.addChoosableFileFilter(filter);
					chooser.setAcceptAllFileFilterUsed(false);
					int option = chooser.showOpenDialog(ArtistEditor.this);
					if (option == JFileChooser.APPROVE_OPTION) {
						File sf = chooser.getSelectedFile();
						String imagePath = sf.getPath();
						BufferedImage pic;
						try {
							pic = ImageIO.read(new File(imagePath));
							picLabel.setText("");
							picLabel.setIcon(new ImageIcon(pic
									.getScaledInstance(150, 150,
											Image.SCALE_FAST)));
							picLabel.setSize(150, 150);
							image = imagePath;
						} catch (IOException e1) {
							picLabel.setText("Geen afbeelding");
							picLabel.setIcon(null);

						}
					}
				}
			});
			JTextArea descriptionTextArea = new JTextArea();
			JLabel label;

			info.add(label = new JLabel("Naam:"));
			label.setBorder(new EmptyBorder(0, 0, 0, 20));
			info.add(name);
			info.add(label = new JLabel("Afbeelding:"));
			label.setBorder(new EmptyBorder(0, 0, 0, 20));
			info.add(selectImage);
			this.add(info, BorderLayout.NORTH);

			JPanel centerPanel = new JPanel(new GridLayout(3, 1));
			centerPanel.add(picLabel);
			centerPanel.add(label = new JLabel("Beschrijving:"));
			centerPanel.add(descriptionTextArea);

			this.add(centerPanel, BorderLayout.CENTER);

			JPanel buttons = new JPanel(new GridLayout(1, 0));
			JButton ok = new JButton("OK");
			buttons.add(ok);
			ok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (a == null) {
						Artist a = new Artist(name.getText(), image,descriptionTextArea.getText());
						FestivalController.instance.getFestivalPlanner().addArtist(a);
					} else {
						a.setImg(image);
						a.setDescription(descriptionTextArea.getText());
						a.setName(name.getText());
					}
					artistEditor.updateItems();
					ArtistEditorDialog.this.dispose();
				}
			});
			JButton cancel = new JButton("Annuleren");
			cancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ArtistEditorDialog.this.dispose();
				}
			});
			buttons.add(cancel);

			buttons.setBorder(new EmptyBorder(10, 0, 10, 0));
			this.add(buttons, BorderLayout.SOUTH);

			pack();
			this.setVisible(true);
		}
	}
}