package screens.editor;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import screens.editor.listpanels.ArtistEditor;
import screens.editor.listpanels.EventEditor;
import screens.editor.listpanels.StageEditor;

public class FestivalEditor extends JPanel {

	ArtistEditor artist = new ArtistEditor();
	EventEditor event = new EventEditor();
	StageEditor stage = new StageEditor();

	public FestivalEditor() {
		setLayout(new BorderLayout());
		JPanel bottom = new JPanel();
		JPanel top = new JPanel();
		top.setLayout(new GridLayout(1, 3, 5, 0));
		// top
		JLabel artistLabel = new JLabel("Artists");
		JLabel eventLabel = new JLabel("Events");
		JLabel stageLabel = new JLabel("Stages");
		top.add(artistLabel);
		top.add(eventLabel);
		top.add(stageLabel);
		// bottom
		bottom.setLayout(new GridLayout(1, 3, 5, 0));

		bottom.add(artist);
		bottom.add(event);
		bottom.add(stage);

		add(top, BorderLayout.NORTH);
		add(bottom, BorderLayout.CENTER);
	}

	public void reload() {
		artist.updateItems();
		event.updateItems();
		stage.updateItems();
	}

	public ArtistEditor getArtistEditor() {
		return artist;
	}

	public EventEditor getEventEditor() {
		return event;
	}

	public StageEditor getStageEditor() {
		return stage;
	}
	
	
}
