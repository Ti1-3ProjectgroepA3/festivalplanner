package screens.planner;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import main.AppSelector;
import models.Event;
import models.FestivalPlanner;
import screens.editor.listpanels.EventEditor;
import controllers.FestivalController;

@SuppressWarnings("serial")
public class PlannerView extends JPanel{
	
	private FestivalController controller = FestivalController.instance;
	private FestivalPlanner festival;
	private ArrayList<Event> events;
	public static final int topPadding = 30, leftPadding = 40,rowSize = 50,columnSize = 200;
	public static int screenHeight;
	private ArrayList<AgendaShape> shapes = new ArrayList<AgendaShape>();
	private int clickedPointYDifference;
	private AgendaShape currentshape;
	private boolean resizing;
	private MutableAttributeSet agendaFont;
	private Rectangle oldRekt;
	private int oldColumn;
	public PlannerView(AppSelector app){
		/*
		 * Font
		 */
		agendaFont = new SimpleAttributeSet();
		StyleConstants.setFontFamily(agendaFont, "Helvetica Neue");
		StyleConstants.setForeground(agendaFont, new Color(255,200,200));
		StyleConstants.setFontSize(agendaFont, 18);
		 
		/*
		 * Main stuff
		 */
		festival = controller.getFestivalPlanner();
		events = festival.getEvents();
		screenHeight = (rowSize * 48) + topPadding;
		setPreferredSize(new Dimension(800,2400));
		setBackground(new Color(18,19,21));
		refreshEvents();
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if(currentshape != null){
					if(currentshape.getEvent().getEndTime().before(currentshape.getEvent().getStartTime())){
						currentshape.setY(topPadding+1);
					}
					for(AgendaShape s: shapes){
						if(currentshape != s){
							if(s.getRect().intersects(oldRekt)){
								currentshape.setRect(oldRekt);
								currentshape.setColumn(oldColumn);
								continue;
							}
						}
					}
				}
				currentshape = null;
				resizing = false;
				refreshEvents();
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				for(AgendaShape s: shapes){
					if(s.getRect().getMaxY()-10 < e.getY() && s.getRect().getMaxY()+10 > e.getY()){
						resizing = true;
					}
					if(s.getRect().contains(e.getPoint())){
						
						clickedPointYDifference = (int) (s.getRect().getY() - e.getY() + topPadding);
						currentshape = s;
						oldRekt = currentshape.getRect();
						oldColumn = currentshape.getColumn();
						return;
					}
				}
				currentshape = null;
			}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				for(AgendaShape s: shapes){
					if(s.getRect().contains(e.getPoint())){
						currentshape = s;
						EventEditor ev = app.getEditor().getEventEditor();
						EventEditor.EventEditorPopup pop = ev.new EventEditorPopup(currentshape.getEvent(), ev);
						refreshEvents();
						return;
					}
				}
				
			}
		});
		addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				for(AgendaShape s: shapes){
					if(s.getRect().contains(e.getPoint())){
						setCursor(new Cursor(Cursor.MOVE_CURSOR));
						return;
					}
				}
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				if(currentshape != null){
					if(currentshape.getRect().getY() < topPadding){
						currentshape.setY(topPadding+1);
					}else if(currentshape.getRect().getMaxY() > (rowSize * 48) + topPadding){
						currentshape.setY((int)((rowSize * 48)+topPadding-currentshape.getRect().getHeight()-1));
					}else{
						if(resizing){
							currentshape.setHeight(e.getY());
							setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
						}else{
							for(int i = 0; i < festival.getStages().size();i++){
								if(e.getX() < ((i+1)*columnSize)+leftPadding){
									currentshape.setColumn(i);
									break;
								}
							}
							currentshape.setY((int)(e.getY()+clickedPointYDifference));
						}
					}
				repaint();
				}
			}
		});
	}
		
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		
		for(int i = 0; i <= 48;i++){
			g2.setColor(new Color(0.2f,0.2f,0.2f,0.2f));	
			if(i%2==0){
				Rectangle rect = new Rectangle(leftPadding,(2* i * rowSize) + topPadding, columnSize*festival.getStages().size(), rowSize*2);
				g2.draw(rect);
				g2.fill(rect);
			}
		}
		
		g2.setColor(new Color(220,220,200));
		for(int i = 0; i <= festival.getStages().size();i++){
			g2.drawLine((columnSize * i) + leftPadding, topPadding, (columnSize * i) + leftPadding, (rowSize * 48) + topPadding);
			if(i < festival.getStages().size()){
				g2.setFont(new Font("Helvetica",Font.PLAIN,18));
				g2.drawString(festival.getStages().get(i).getName(), (columnSize * i) + leftPadding, 22);
			}
		}
		g2.setFont(new Font("Helvetica",Font.PLAIN,18));
		g2.setColor(new Color(220,220,220,200));
		for(int i = 0; i <= 48;i++){
			if(i%2==0){
				g2.setStroke(new BasicStroke());
				g2.drawString(""+ i/2, 16, (i * rowSize) + topPadding + 16);
			}else{
				g2.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0)); 
			}
			g2.drawLine(leftPadding, (i * rowSize) + topPadding, (festival.getStages().size() * columnSize) + leftPadding, (i * rowSize) + topPadding);
		}
		
		for(AgendaShape s: shapes){
			g2.setColor(new Color(1.0f,0.2f,0.2f,0.8f));	
			s.draw(g2);
			g2.setFont(new Font("Helvetica Neue",Font.PLAIN,12));
			g2.setColor(Color.white);
			g2.drawString("Start: " + s.getEvent().getStartTimeString() + ", End: " + s.getEvent().getEndTimeString(),(int) s.getRect().getX()+5,(int) s.getRect().getY()+15);
		}
		setPreferredSize(new Dimension((columnSize * festival.getStages().size() + 25),screenHeight+31));
	}
	public void refreshEvents(){
		shapes.clear();
		for(Event e:events){
			shapes.add(new AgendaShape(e));
		}
		System.out.println("test refresh");
		repaint();
	}
}
