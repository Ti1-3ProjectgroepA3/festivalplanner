package screens.planner;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Calendar;

import models.Event;
import models.FestivalPlanner;
import controllers.FestivalController;

public class AgendaShape {
	private Rectangle rect;

	private FestivalController controller = FestivalController.instance;
	private FestivalPlanner festival;
	private Event event;


	private int column, minuteStart, minuteEnd;
	
	public AgendaShape(Event e){
		festival = controller.getFestivalPlanner();
		event = e;
		column = festival.getStages().indexOf(e.getStage());
		
		minuteStart = e.getStartTime().get(Calendar.MINUTE) + (e.getStartTime().get(Calendar.HOUR_OF_DAY) * 60);
		minuteEnd = e.getEndTime().get(Calendar.MINUTE) + (e.getEndTime().get(Calendar.HOUR_OF_DAY) * 60);
		rect = new Rectangle((column * PlannerView.columnSize) + PlannerView.leftPadding,
				(int)(((minuteStart / 30.0) * PlannerView.rowSize)+PlannerView.topPadding),
				PlannerView.columnSize,
				(int)((minuteEnd - minuteStart)/30.0)*PlannerView.rowSize);
	}
	public void setY(int y){
		getRect().setLocation((int)getRect().getX(), y);
		int startminutes = (y- PlannerView.topPadding) * 30 / PlannerView.rowSize;
		int endminutes = (int) ((getRect().getMaxY()- PlannerView.topPadding) * 30 / PlannerView.rowSize);
		event.getStartTime().set(Calendar.MINUTE, startminutes%60);
		event.getStartTime().set(Calendar.HOUR_OF_DAY, Math.round(startminutes/60));
		event.getEndTime().set(Calendar.MINUTE, endminutes%60 );
		event.getEndTime().set(Calendar.HOUR_OF_DAY, Math.round(endminutes/60));
	}
	public void setHeight(int h){
		if(h-getRect().getY() > 10){
			getRect().setSize((int)getRect().getWidth(), (int) (h-getRect().getY()));
			int endminutes = (int) ((getRect().getMaxY()- PlannerView.topPadding) * 30 / PlannerView.rowSize);
			event.getEndTime().set(Calendar.MINUTE, endminutes%60 );
			event.getEndTime().set(Calendar.HOUR_OF_DAY, Math.round(endminutes/60));
		}
	}
	
	public Rectangle getRect() {
		return rect;
	}
	
	public int getColumn() {
		return column;
	}
	public void setColumn(int column) {
		getRect().setLocation((column * PlannerView.columnSize) + PlannerView.leftPadding, (int)getRect().getY());
		event.setStage(festival.getStages().get(column));
		this.column = column;
	}
	
	public Event getEvent() {
		return event;
	}
	
	public void setRect(Rectangle rect) {
		this.rect = rect;
	}
	public void draw(Graphics2D g2){
		g2.draw(getRect());
		g2.fill(getRect());		
	}
}
