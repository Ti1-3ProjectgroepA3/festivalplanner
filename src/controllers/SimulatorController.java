package controllers;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import simulator.view.sidebar.properties.Con_Properties;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.DrawObject;
import simulator.view.simulator.buildings.TekenObject;
import simulator.view.simulator.buildings.area.AreaWaypoint;
import simulator.view.simulator.buildings.food.Burger;
import simulator.view.simulator.buildings.food.Coffee;
import simulator.view.simulator.buildings.food.Drink;
import simulator.view.simulator.buildings.food.Hotdog;
import simulator.view.simulator.buildings.food.Ice;
import simulator.view.simulator.buildings.food.Noodles;
import simulator.view.simulator.buildings.misc.Ingang;
import simulator.view.simulator.buildings.stages.Podium;
import simulator.view.simulator.buildings.utility.EHBO;
import simulator.view.simulator.buildings.utility.MuntjesVerkoper;
import simulator.view.simulator.buildings.utility.Toilet;
import simulator.view.simulator.visitors.Visitor;
import models.Stage;

public class SimulatorController {

	ArrayList<TekenObject> buildObjects;
	ArrayList<TekenObject> simulatorObjects = new ArrayList<>();
	ArrayList<Visitor> visitors = new ArrayList<>();
	ArrayList<AreaWaypoint> areas = new ArrayList<>();
	ConSimulator sim;
	Con_Properties props;

	public SimulatorController(ConSimulator sim, Con_Properties props) {
		this.sim = sim;
		this.props = props;
		buildObjects = new ArrayList<TekenObject>();
		ArrayList<models.Stage> stages = FestivalController.instance
				.getFestivalPlanner().getStages();
		buildObjects.add(new Drink());
		buildObjects.add(new Coffee());
		buildObjects.add(new Burger());
		buildObjects.add(new Hotdog());
		buildObjects.add(new Noodles());
		buildObjects.add(new Ice());
		buildObjects.add(new Ingang());
		buildObjects.add(new EHBO());
		buildObjects.add(new MuntjesVerkoper());
		buildObjects.add(new Toilet());
		if (stages.size() != 0) {
			for (int i = 0; i < stages.size(); i++) {
				buildObjects.add(new Podium(stages.get(i), i));
			}
		}
		for (TekenObject o : buildObjects) {
			o.setConProperties(props);
			o.setSimulator(sim);
		}
	}

	public ArrayList<TekenObject> getBuildObjects() {
		return buildObjects;
	}

	public ArrayList<TekenObject> getSimulatorObjects() {
		return simulatorObjects;
	}

	public ArrayList<Visitor> getVisitors() {
		return visitors;
	}

	public ArrayList<AreaWaypoint> getAreas() {
		return areas;
	}

	public DrawObject[] getDrawObjects() {
		List<DrawObject> list = new LinkedList<>();
		list.addAll(simulatorObjects);
		list.addAll(visitors);
		list.addAll(areas);
		return list.toArray(new DrawObject[0]);
	}

	public void add(DrawObject drawObject) {
		if (drawObject instanceof TekenObject)
			simulatorObjects.add((TekenObject) drawObject);
		if (drawObject instanceof Visitor)
			visitors.add((Visitor) drawObject);
		if (drawObject instanceof AreaWaypoint)
			areas.add((AreaWaypoint) drawObject);

	}

	public ArrayList<AreaWaypoint> generatePath(Point2D form, AreaWaypoint to) {
		ArrayList<AreaWaypoint> path = new ArrayList<>();

		Point2D lastPoint = form;
		boolean pathFound = false;
		if (!pathHasCollsion(lastPoint, to)) {
			path.add(to);
			pathFound = true;
		}

		ArrayList<AreaWaypoint> areaWaypointToGo = (ArrayList<AreaWaypoint>) areas
				.clone();
		while (!pathFound) {
			ArrayList<AreaWaypoint> areaWaypoints = (ArrayList<AreaWaypoint>) areaWaypointToGo
					.clone();

			while (areaWaypoints.size() > 0) {
				AreaWaypoint closedPoint = getClosedWaypoint(areaWaypoints,
						lastPoint);
				if (!closedPoint.attachedToTekenObject()
						&& !pathHasCollsion(lastPoint, closedPoint)) {
					path.add(closedPoint);
					areaWaypointToGo.remove(closedPoint);
					lastPoint = closedPoint.getCenterPoint();
					System.out.println("Path to crossroad area found");
					break;
				}
				areaWaypoints.remove(closedPoint);
			}
			if (areaWaypoints.size() == 0)
				return null;

			if (!pathHasCollsion(lastPoint, to)) {
				path.add(to);
				pathFound = true;
				System.out.println("Path to dest area found");
			}

		}
		return path;
	}

	public AreaWaypoint getClosedWaypoint(
			ArrayList<AreaWaypoint> areaWaypoints, Point2D point) {
		AreaWaypoint area = areaWaypoints.get(0);

		double closedDistance = area.getCenterPoint().distance(point);

		for (AreaWaypoint a : areaWaypoints) {
			if (closedDistance < a.getCenterPoint().distance(point)) {
				area = a;
				closedDistance = a.getCenterPoint().distance(point);
			}

		}
		return area;

	}

	private boolean pathHasCollsion(Point2D form, AreaWaypoint to) {

		boolean pathFound = true;
		Line2D s = new Line2D.Double(form, to.getCenterPoint());
		System.out.print("Form: " + form.getX() + "," + form.getY());
		System.out.println("  To: " + to.getCenterPoint().getX() + ","
				+ to.getCenterPoint().getY());
		for (TekenObject obj : simulatorObjects) {
			if (obj.checkCollsion(s)) {
				pathFound = false;
				break;
			}
		}
		// try {
		// Thread.sleep(750);
		// } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		return !pathFound;
	}

	public void exportSimulator(String file) {
		DrawObject[] objects = getDrawObjects();
		try {
			FileOutputStream fileOut = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(objects);
			out.close();
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void importSimulator(String file) {
		DrawObject[] objects = null;
		try {
			FileInputStream fileIn = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			objects = (DrawObject[]) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
			return;
		}
		simulatorObjects.clear();
		visitors.clear();
		areas.clear();
		for (DrawObject o : objects) {
			if (o instanceof TekenObject) {
				((TekenObject) o).loadImage();
				((TekenObject) o).setConProperties(props);
				((TekenObject) o).setSimulator(sim);
				if (o instanceof Podium) {
					boolean stageFound = false;
					for (Stage s : FestivalController.instance
							.getFestivalPlanner().getStages()) {
						if(((Podium) o).getStage() != null) {
						if (s.getName().equals(
								((Podium) o).getStage().getName())) {
							((Podium) o).setStage(s);
							stageFound = true;
							break;
						}
						} else {
							System.out.println("??");
						}
					}
					if (!stageFound) {
						continue;
					}
				}
			}
			System.out.println(o);
			add(o);

		}
	}

	public void remove(DrawObject drawObject) {
		if (drawObject instanceof TekenObject)
			simulatorObjects.remove((TekenObject) drawObject);
		if (drawObject instanceof Visitor)
			visitors.remove((Visitor) drawObject);
		if (drawObject instanceof AreaWaypoint)
			areas.remove((AreaWaypoint) drawObject);

	}
}