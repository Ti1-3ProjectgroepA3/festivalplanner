package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import models.Artist;
import models.FestivalPlanner;

public class FestivalController {

	private FestivalPlanner festivalPlanner;
	public static FestivalController instance = new FestivalController();

	private FestivalController() {
		newFestival();
	}

	public void newFestival() {
		festivalPlanner = new FestivalPlanner();		
	}

	public FestivalPlanner getFestivalPlanner() {
		return festivalPlanner;
	}

	public void saveToFile(String file) {
		try {
			FileOutputStream fileOut = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(festivalPlanner);
			out.close();
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadToFile(String file) {
		try {
			FileInputStream fileIn = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			festivalPlanner = (FestivalPlanner) in.readObject();
			in.close();
			fileIn.close();
		} catch(IOException i){
			i.printStackTrace();
			return;
		} catch(ClassNotFoundException c) {
	        c.printStackTrace();
	        return;
	    }
	}
}

