package simulator;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

import simulator.view.sidebar.Con_Sidebar;
import simulator.view.simulator.ConSimulator;
import simulator.view.toolbar.Con_Toolbar;

public class ContentPane extends JPanel{
	private ConSimulator sim;
	private Con_Toolbar toolbar;
	private Con_Sidebar sidebar;
	public ContentPane(){
		sim = new ConSimulator();
		toolbar = new Con_Toolbar(sim);
		sidebar = new Con_Sidebar(sim);
		setPreferredSize(new Dimension(800,600));
		setLayout(new BorderLayout());
		add(toolbar, BorderLayout.NORTH);
		add(sim, BorderLayout.CENTER);
		add(sidebar, BorderLayout.EAST);
		
		
		
	}
	public ConSimulator getSim() {
		return sim;
	}
	public Con_Toolbar getToolbar() {
		return toolbar;
	}
	public Con_Sidebar getSidebar() {
		return sidebar;
	}
	
}
