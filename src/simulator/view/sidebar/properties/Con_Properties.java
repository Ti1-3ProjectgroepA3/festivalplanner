package simulator.view.sidebar.properties;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import models.Artist;
import models.Event;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.buildings.TekenObject;
import simulator.view.simulator.buildings.stages.Podium;
import simulator.view.simulator.events.SimulatorMouseListener;
import controllers.FestivalController;

public class Con_Properties extends JPanel {
	static TekenObject objectInfo;
	String objectNaam;
	// static JTextArea text = new JTextArea(10, 22);
	static JLabel naam = new JLabel("  Naam: ");
	static JLabel event = new JLabel("  Categorie: ");
	static JLabel pop = new JLabel("  Populariteit: ");
	static JLabel rotatie = new JLabel("  Rotatie: ");
	static JLabel schaal = new JLabel("  Schaal: ");
	static JLabel currentEventLabel = new JLabel("  Current event: ");

	JButton set = new JButton("Set Description");
	private ConSimulator sim;
	private FestivalController agenda = FestivalController.instance;

	public Con_Properties(ConSimulator sim) {
		setBackground(new Color(34, 35, 38));
		setLayout(new GridLayout(6, 1));
		setPreferredSize(new Dimension(getWidth(), 200));
		this.sim = sim;
		naam.setForeground(Color.white);
		add(naam);
		event.setForeground(Color.white);
		add(event);
		rotatie.setForeground(Color.white);
		add(rotatie);
		schaal.setForeground(Color.white);
		add(schaal);
		pop.setForeground(Color.white);
		add(pop);
		currentEventLabel.setForeground(Color.white);
		add(currentEventLabel);

	}
	public static void giveInfo()
	{
		objectInfo = SimulatorMouseListener.getCurrentObject();

		DecimalFormat df = new DecimalFormat("#.00");

		naam.setText("  Naam: " + objectInfo.getNaam());
		event.setText("  Categorie: " + objectInfo.getCategorie());
		if (objectInfo instanceof Podium) {
			Podium podium = ((Podium) objectInfo);
			Event currentEvent = podium.getCurrentEvent();
			if (currentEvent != null) {
				String text = "   Current event: ";
				for (Artist a : currentEvent.getArtists()) {
					text += a.getName() + ", ";
				}
				text = text.substring(0, text.length() - 2);
				currentEventLabel.setText(text);
			}
			pop.setText("  Populariteit: " + objectInfo.getPopularty());
		} else {
			pop.setText("  Populariteit: " + objectInfo.getPopularty());
			currentEventLabel.setText("");
		}
		rotatie.setText("  Rotatie: " + df.format(objectInfo.getRotation()));
		schaal.setText("  Schaal: " + df.format(objectInfo.getScale()));
	}

	public void update(TekenObject dragObject) {

	}
}