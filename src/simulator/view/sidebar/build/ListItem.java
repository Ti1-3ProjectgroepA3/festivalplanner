package simulator.view.sidebar.build;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.DrawObject;
import simulator.view.simulator.buildings.TekenObject;
import simulator.view.simulator.buildings.misc.Ingang;
import simulator.view.simulator.buildings.stages.Podium;
import simulator.view.simulator.buildings.misc.Ingang;

public class ListItem extends JPanel {
	private Image image;
	private String naam;
	private ConSimulator sim;

	public ListItem(ConSimulator sim,Image image, String naam, TekenObject tekenObject) {
		setPreferredSize(new Dimension(216, 68));
		this.image = image;
		this.naam = naam;
		this.sim = sim;
		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (tekenObject instanceof Podium) {
					sim.add(tekenObject);
				} else {
					try {
						sim.add((DrawObject) tekenObject.clone());
					} catch (CloneNotSupportedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		g2.drawImage(image, 2, 2, 64, 64, null);
		g2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
		g2.drawString(naam, 72, 16);
	}
}
