package simulator.view.sidebar.build;

import java.util.ArrayList;

import javax.swing.JPanel;

import simulator.view.customUI.WrapLayout;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.buildings.TekenObject;
import controllers.SimulatorController;

public class Lst_Food extends JPanel{
	ArrayList<TekenObject> objects;
	public Lst_Food(ConSimulator sim) {
		setLayout(new WrapLayout(WrapLayout.LEFT));
		objects = sim.getSimulatorController().getBuildObjects(); 
		for(TekenObject o : sim.getSimulatorController().getBuildObjects()){
			if(o.getCategorie() == "food"){
				add(new ListItem(sim,o.getImage(), o.getNaam(),o));
			}
		}
	}
}
