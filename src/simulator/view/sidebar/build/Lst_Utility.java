package simulator.view.sidebar.build;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JPanel;

import controllers.SimulatorController;
import simulator.view.customUI.WrapLayout;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.buildings.TekenObject;

public class Lst_Utility extends JPanel{
	ArrayList<TekenObject> objects;
	public Lst_Utility(ConSimulator sim) {
		objects = sim.getSimulatorController().getBuildObjects(); 
		setLayout(new WrapLayout(WrapLayout.LEFT));
		for(TekenObject o : objects){
			if(o.getCategorie() == "util"){
				add(new ListItem(sim,o.getImage(), o.getNaam(),o));
			}
		}
	}
}
