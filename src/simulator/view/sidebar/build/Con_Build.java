package simulator.view.sidebar.build;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import simulator.view.simulator.ConSimulator;
import main.Main;

public class Con_Build extends JPanel{
	public Con_Build(ConSimulator sim){
		super(new GridLayout(1, 1));
		ImageIcon icon = createImageIcon("/images/deal-with-it.png");
		JTabbedPane tabbedPane = new JTabbedPane();
		
		tabbedPane.addTab("Stage",(Icon)icon,new JScrollPane(new Lst_Stages(sim)));
		tabbedPane.addTab("Food",(Icon)icon,new JScrollPane(new Lst_Food(sim)));
		tabbedPane.addTab("Utility",(Icon)icon,new JScrollPane(new Lst_Misc(sim)));
		tabbedPane.addTab("Misc",(Icon)icon,new JScrollPane(new Lst_Utility(sim)));
		
		for(Component c : tabbedPane.getComponents()) {
            if (c instanceof JScrollPane) {
                JScrollPane scrollPane = (JScrollPane) c;
                scrollPane.getVerticalScrollBar().setUnitIncrement(20);
            }
        }
		
		add(tabbedPane);
	}
	 protected static ImageIcon createImageIcon(String path) {
	        java.net.URL imgURL = Main.class.getResource(path);
	        if (imgURL != null) {
	            return new ImageIcon(imgURL);
	        } else {
	            System.err.println("Couldn't find file: " + path);
	            return null;
	        }
	    }
}
