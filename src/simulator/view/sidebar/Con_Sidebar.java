package simulator.view.sidebar;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

import simulator.view.sidebar.build.Con_Build;
import simulator.view.sidebar.properties.Con_Properties;
import simulator.view.simulator.ConSimulator;

public class Con_Sidebar extends JPanel{
	public Con_Sidebar(ConSimulator sim){
		setPreferredSize(new Dimension(250,getHeight()));
		setLayout(new BorderLayout());
		JPanel top = new JPanel(new BorderLayout());
		top.add(new Con_Properties(sim),BorderLayout.NORTH);
		add(top, BorderLayout.NORTH);
		add(new Con_Build(sim),BorderLayout.CENTER);
	}
}
