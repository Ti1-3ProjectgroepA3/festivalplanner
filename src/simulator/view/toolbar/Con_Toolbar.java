package simulator.view.toolbar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import simulator.view.customUI.WrapLayout;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.timer.TimerThread;

public class Con_Toolbar extends JPanel{
	private int minutes;
	private int seconds;
	private TimerThread timer;	
	
	private JButton areaNew, deleteBut, zoomIn, zoomOut, zoomDef,play,pause,stop, spawnVisitors;
	private JSpinner visitors;
	private JLabel area,delete,zooms,simulationCrtl,setVisitors,festivalTime;

	private ConSimulator sim;
	public Con_Toolbar(ConSimulator sim){
		timer = new TimerThread(sim,sim.getSimulatorController());
		this.sim = sim;
		sim.setTimer(timer);
		setLayout(new WrapLayout(WrapLayout.LEFT));
		
		ArrayList<JComponent> comps = new ArrayList<>();
	
		area = new JLabel("Area: "+ sim.getAreaMode());
		areaNew = new JButton();
		delete = new JLabel("Delete: " + sim.getDeleteMode());
		deleteBut = new JButton();
		zooms = new JLabel("Zoom: ");
		zoomIn = new JButton();
		zoomOut = new JButton();
		zoomDef = new JButton();
		simulationCrtl = new JLabel("Simulation: ");
		play = new JButton();
		pause = new JButton();
		stop = new JButton();
		setVisitors = new JLabel("Max visitors: ");
		visitors = new JSpinner();
		festivalTime = new JLabel("Time: " + minutes + ":" + seconds);
		spawnVisitors = new JButton();
		
		try {
			areaNew.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/area.png"))));
			deleteBut.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/delete.png"))));
			zoomIn.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/zoom-in.png"))));
			zoomOut.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/zoom-out.png"))));
			zoomDef.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/zoom-def.png"))));
			play.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/play.png"))));
			pause.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/pause.png"))));
			stop.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/stop.png"))));
			spawnVisitors.setIcon(new ImageIcon(ImageIO.read(new File("images/icons/persons.png"))));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		comps.add(area);
		comps.add(areaNew);
		comps.add(delete);
		comps.add(deleteBut);
		comps.add(zooms);
		comps.add(zoomIn);
		comps.add(zoomOut);
		comps.add(zoomDef);
		comps.add(simulationCrtl);
		comps.add(play);
		comps.add(pause);
		comps.add(stop);
		comps.add(setVisitors);
		visitors.setPreferredSize(new Dimension(64,32));
		comps.add(visitors);
		comps.add(spawnVisitors);
		comps.add(festivalTime);
		for(JComponent c: comps){
			if(c.getClass() == areaNew.getClass())
				c.setPreferredSize(new Dimension(32,32));
			c.setForeground(Color.white);
			add(c);
		}
		setBackground(new Color(34,35,38));
		addListeners();
	}
	
	private void addListeners(){
		/*
		 * NEW PATH
		 */
		deleteBut.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sim.toggleDeleteMode();
				delete.setText("Delete: " + sim.getDeleteMode());
				area.setText("Area: " + sim.getAreaMode());
			}
		});
		/*
		 * NEW AREA
		 */
		areaNew.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sim.toggleAreaMode();
				area.setText("Area: " + sim.getAreaMode());
				delete.setText("Delete: " + sim.getDeleteMode());
			}
		});
		/*
		 * ZOOMS 
		 */
		
		zoomIn.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				double scale = sim.getCameraScale();
				scale *= 1.1;
				sim.setCameraScale(scale);
			}
		});
		
		zoomOut.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				double scale = sim.getCameraScale();
				scale *= 0.9;
				sim.setCameraScale(scale);
			}
		});
		zoomDef.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sim.setCameraScale(1);
				sim.setCameraPoint(new Point2D.Double(sim.getScreenWidth()/2,sim.getScreenHeight()/2));
			}
		});
		
		/*
		 * SIMULATION
		 */
		
		play.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ConSimulator.running = true;
				
				try {
					updateTime();
					timer.start();
				} catch (Exception ex) {
					System.out.println(ex);
				}
				
				timer.running = true;
			}
		});
		
		pause.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ConSimulator.running = false;
				sim.pauseSpawn();
				try{
					timer.running = false;
				} catch(Exception ex){
					System.out.println(ex);
				}
			}
		});
		stop.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ConSimulator.running = false;
				try{
					sim.clearStopSpawn();
					timer.resetTimer();
				} catch(Exception ex){
					System.out.println(ex);
				}
			}
		});
		spawnVisitors.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				if (!ConSimulator.running){
					JOptionPane.showMessageDialog(null,
							"Simulator moet actief zijn voordat bezoekers toegevoegd kunnen worden.");
				}
				else {
					sim.spawnVisitors((int) visitors.getValue());
					System.out.println("Max Visitors: " + visitors.getValue());
				}
			}
		});
	}
	
	private void updateTime(){
		Thread time = new Thread(new Runnable(){
			public void run(){
				while (true){
					try{
						seconds = timer.getTimeSeconds();
						minutes = timer.getTimeMinutes();
						festivalTime.setText(String.format("Time: %02d:%02d",minutes, seconds));
						festivalTime.repaint();
						Thread.sleep(100);
					} catch(Exception e){
						System.out.println(e);
					}
				}
			}
		});
		time.start();
	}

	public TimerThread getTimer() {
		return timer;
	}

	public void setTimer(TimerThread timer) {
		this.timer = timer;
	}

	
	
}
