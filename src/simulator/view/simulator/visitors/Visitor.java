package simulator.view.simulator.visitors;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import controllers.SimulatorController;
import simulator.view.simulator.DrawObject;
import simulator.view.simulator.buildings.area.AreaWaypoint;

public class Visitor extends DrawObject {
	private final static String IMAGES[] = { "images/people/person1.png",
			"images/people/person2.png", "images/people/person3.png",
			"images/people/person4.png", "images/people/person5.png" };

	Point2D positie;
	double rotation;
	double speed;
	Point2D currentTarget;
	AreaWaypoint currentTargetedArea;
	ArrayList<AreaWaypoint> targets;

	BufferedImage image;

	boolean targetReached = false;
	SimulatorController controller;

	public Visitor(SimulatorController controller, Point2D positie)
			throws IOException {
		this.controller = controller;
		this.positie = positie;
		this.rotation = Math.random() * 360;
		this.speed = 1;
		this.currentTarget = new Point2D.Double(400, 300);
		image = ImageIO.read(new File(IMAGES[(int) (Math.random() * 5)]));

	}

	public void update(ArrayList<Visitor> andereVisitors, double speed) {
		// rotation+=0.01;
		speed = this.speed + 0.3 + (speed);
		Point2D difference = new Point2D.Double(currentTarget.getX()
				- positie.getX(), currentTarget.getY() - positie.getY());

		double newRotation = Math.atan2(difference.getY(), difference.getX());
		double rotDifference = rotation - newRotation;
		while (rotDifference > Math.PI)
			rotDifference -= 2 * Math.PI;
		while (rotDifference < -Math.PI)
			rotDifference += 2 * Math.PI;

		if (Math.abs(rotDifference) < 0.1)
			rotation = newRotation;
		else if (rotDifference < 0)
			rotation += 0.1;
		else if (rotDifference > 0)
			rotation -= 0.1;

		Point2D oldPositie = positie;

		positie = new Point2D.Double(positie.getX() + speed
				* Math.cos(rotation), positie.getY() + speed
				* Math.sin(rotation));

		if (currentTargetedArea == null && positie.distance(currentTarget) < 30)
			targetReached = true;
		else if (currentTargetedArea != null && currentTargetedArea.contains(positie)) {
			if (targets.indexOf(currentTargetedArea) + 1 == targets.size()) {
				targetReached = true;
			} else {
				System.out.println("Start nieuwe target");
				currentTarget = targets.get(
						targets.indexOf(currentTargetedArea) + 1)
						.getCenterPoint();
				currentTargetedArea = targets.get(targets
						.indexOf(currentTargetedArea) + 1);
			}
		}

		if (hasCollision(andereVisitors)) {
			positie = oldPositie;
			rotation += 0.2;
		}

	}

	public void draw(Graphics2D g) {
		AffineTransform tx = new AffineTransform();
		tx.translate(positie.getX() - 8, positie.getY() - 8);
		tx.rotate(rotation, 8, 8);

		g.setColor(Color.blue);
		g.drawImage(image, tx, null);
	}

	public boolean hasCollision(ArrayList<Visitor> bezoekers) {
		for (Visitor b : bezoekers) {
			if (b == this)
				continue;
			if (b.positie.distance(positie) < 16)
				return true;
		}
		return false;
	}

	public void setTarget(AreaWaypoint target) {
		this.targets = this.controller.generatePath(positie, target);
		if (targets == null) {
			System.out.println("No path");
			targetReached = true;
			return;
		}
		this.currentTarget = targets.get(0).getCenterPoint();
		this.currentTargetedArea = targets.get(0);
		targetReached = false;
	}

	public void setTarget(Point2D target) {
		this.currentTarget = target;

		targetReached = false;
	}

	public boolean isTargetReached() {
		return targetReached;
	}

	public Point2D getTarget() {
		return currentTarget;
	}

	public AreaWaypoint getTargetArea() {
		return currentTargetedArea;
	}
}