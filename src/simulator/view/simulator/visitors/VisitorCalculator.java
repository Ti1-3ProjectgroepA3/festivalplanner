package simulator.view.simulator.visitors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import models.Event;
import models.Stage;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.buildings.area.AreaWaypoint;
import controllers.FestivalController;
import controllers.SimulatorController;

public class VisitorCalculator {

	ConSimulator sim;
	SimulatorController simulatorController;
	Random random;

	static DateFormat f = new SimpleDateFormat("HH:mm:ss.SSS");

	public VisitorCalculator(ConSimulator sim,
			SimulatorController simulatorController) {
		random = new Random();
		this.sim = sim;

		this.simulatorController = simulatorController;
	}

	public AreaWaypoint getRandomTarget() {
		List<AreaWaypoint> list = getList();
		return list.get(random.nextInt(list.size()));
	}

	public ArrayList<AreaWaypoint> getList() {
		ArrayList<AreaWaypoint> list = new ArrayList<>();

		for (AreaWaypoint w : simulatorController.getAreas()) {
			if (w.attachedToTekenObject()) {
				
				for (int i = 0; i < w.getTekenObject().getPopularty(); i++)
					list.add(w);
			}
		}

		return list;
	}

	public static Event getCurrentEvent(Stage s, int time) {

		Calendar c = new GregorianCalendar(0, 1, 0, 0, 0, time);
		Date d = c.getTime();

		for (Event e : FestivalController.instance.getFestivalPlanner()
				.getEvents()) {
			if (e.getStage() != s)
				continue;
			if (f.format(e.getStartTime().getTime()).compareTo(f.format(d)) <= 0
					&& f.format(e.getEndTime().getTime())
							.compareTo(f.format(d)) >= 0) {
				return e;
			}
		}
		return null;
	}

}
