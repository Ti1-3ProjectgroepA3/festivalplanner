package simulator.view.simulator.events;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import simulator.view.sidebar.properties.Con_Properties;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.DrawObject;
import simulator.view.simulator.buildings.TekenObject;
import simulator.view.simulator.buildings.area.AreaWaypoint;
import simulator.view.simulator.visitors.Visitor;
import controllers.SimulatorController;

public class SimulatorMouseListener implements MouseListener,
		MouseWheelListener, MouseMotionListener {
	private ConSimulator simulator;
	private SimulatorController controller;

	private DrawObject dragObject = null;
	static TekenObject currentObject;

	public SimulatorMouseListener(ConSimulator simulator,
			SimulatorController controller) {
		this.simulator = simulator;
		this.controller = controller;
	}

	private Point2D lastClickPosition;
	private Point lastMousePosition;

	public void mousePressed(MouseEvent e) {
		Point2D clickPoint = simulator.getClickPoint(e.getPoint());
		lastClickPosition = clickPoint;
		lastMousePosition = e.getPoint();

		for (TekenObject o : controller.getSimulatorObjects()) {
			if (o.contains(clickPoint)) {
				dragObject = o;
				currentObject = o;
				getCurrentObject();
				Con_Properties.giveInfo();
			}
		}

		if(simulator.getDeleteMode() && dragObject!=null){
			controller.remove(dragObject);
			System.out.println(dragObject.toString());
		}
		
		if (simulator.getAreaMode()) {
			if (dragObject instanceof TekenObject) {
				dragObject = new AreaWaypoint((TekenObject) dragObject);
			} else
				dragObject = new AreaWaypoint(clickPoint);

			controller.add(dragObject);
			simulator.setAreaMode(false);

		} else if (dragObject instanceof AreaWaypoint) {
			dragObject = null;
		}

	}

	public static TekenObject getCurrentObject() {
		return currentObject;

	}

	public void mouseReleased(MouseEvent e) {
		Point2D clickPoint = simulator.getClickPoint(e.getPoint());

		if (this.dragObject instanceof TekenObject) {

			TekenObject dragObject = (TekenObject) this.dragObject;
			dragObject.mouseReleased(e, clickPoint);
			for(Visitor v : controller.getVisitors()) {
				if(v.getTargetArea() != null)
					v.setTarget(v.getTargetArea());
			}
		} else
			return;

		this.dragObject = null;
		simulator.repaint();
	}

	public void mouseDragged(MouseEvent e) {
		Point2D clickPoint = simulator.getClickPoint(e.getPoint());
		if (dragObject instanceof TekenObject) {
			((TekenObject) this.dragObject).mouseDragged(e, clickPoint,
					lastClickPosition);
			simulator.repaint();
		} else {
			simulator.setCameraPoint(new Point2D.Double(simulator
					.getCameraPoint().getX()
					+ (lastMousePosition.getX() - e.getX()), simulator
					.getCameraPoint().getY()
					+ (lastMousePosition.getY() - e.getY())));
			simulator.repaint();
		}
		lastMousePosition = e.getPoint();
		lastClickPosition = clickPoint;
	}

	public void mouseMoved(MouseEvent e) {
		Point2D clickPoint = simulator.getClickPoint(e.getPoint());
		if (dragObject instanceof TekenObject) {
			((TekenObject) this.dragObject).mouseMoved(e, clickPoint,
					lastClickPosition);
			simulator.repaint();
		} else if (dragObject instanceof AreaWaypoint) {

			AreaWaypoint areaWaypoint = (AreaWaypoint) this.dragObject;
			if (areaWaypoint.attachedToTekenObject()) {
				areaWaypoint.setHeight((clickPoint.getY() / areaWaypoint.getTekenObject().getScale())
						- (areaWaypoint.getTekenObject().position.getY() )
						- (areaWaypoint.getTekenObject().getHeight() ));
				System.out.println(areaWaypoint.getHeight());
			}
			simulator.repaint();

		}
		lastMousePosition = e.getPoint();
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		Point2D clickPoint = simulator.getClickPoint(e.getPoint());

		lastClickPosition = clickPoint;
		lastMousePosition = e.getPoint();
		for (TekenObject o : controller.getSimulatorObjects()) {
			if (o.contains(clickPoint)) {
				o.scale *= 1 + (e.getPreciseWheelRotation() / 10.0);
				simulator.repaint();
				return;
			}
		}
		double cameraScale = simulator.getCameraScale();
		cameraScale *= 1 - (e.getPreciseWheelRotation() / 20.0);
		simulator.setCameraScale(cameraScale);
		simulator.repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		for (TekenObject o : controller.getSimulatorObjects()) {
			if (o.position == null) {
				o.position = simulator.getClickPoint(e.getPoint());
				o.position = new Point2D.Double(o.position.getX()
						+ (o.getWidthNoScale() * simulator.getCameraScale()),
						o.position.getY()
								+ (o.getWidth() * simulator.getCameraScale()));
				lastClickPosition = o.position;
				this.dragObject = o;
				return;
			}
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
