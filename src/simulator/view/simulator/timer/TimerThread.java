package simulator.view.simulator.timer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import models.Event;
import models.Stage;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.buildings.stages.Podium;
import simulator.view.simulator.visitors.Visitor;
import simulator.view.simulator.visitors.VisitorCalculator;
import controllers.FestivalController;
import controllers.SimulatorController;

public class TimerThread extends Thread {
	ConSimulator sim;
	SimulatorController simulatorController;
	VisitorCalculator calcuator;
	double speed = 0.1;

	double time = 0;

	public TimerThread(ConSimulator sim, SimulatorController simulatorController) {
		this.sim = sim;
		this.simulatorController = simulatorController;
		this.calcuator = sim.getVisitorCalculator();
	}

	public boolean running = false;

	@Override
	public void run() {
		while (true) {
			while (running) {
				try {
					time += speed * 40;
					Iterator<Visitor> it = simulatorController.getVisitors()
							.iterator();
					while (it.hasNext()) {
						Visitor v = it.next();
						if (ConSimulator.running == true) {
							v.update(simulatorController.getVisitors(), speed);
							boolean eventFound = false;
							
							if (v.getTargetArea().getTekenObject() instanceof Podium) {
								Podium podium = ((Podium) v.getTargetArea()
										.getTekenObject());
								
								if (podium != null && podium.getCurrentEvent() != null) {
									eventFound = true;
								}
							}

							while (!eventFound && v.isTargetReached()) {
								v.setTarget(calcuator
										.getRandomTarget());
							}
						}

					}
					Thread.sleep(1000 / 60);
					// System.out.println(minutes + ":" + seconds);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			try {
				Thread.sleep(1000 / 60);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void resetTimer() {
		time = 0;
		running = false;
	}

	public double getTime() {
		return time;
	}

	public int getTimeSeconds() {
		int time = (int) this.time;
		return time/60 % 60;
	}

	public int getTimeMinutes() {
		int time = (int) this.time;
		return (time /60 / 60) % 60;
	}

}
