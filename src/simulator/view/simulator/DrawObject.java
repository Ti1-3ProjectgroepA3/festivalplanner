package simulator.view.simulator;

import java.awt.Graphics2D;
import java.io.Serializable;

public abstract class DrawObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 499348179465546071L;

	public abstract void draw(Graphics2D g2);
 
}
