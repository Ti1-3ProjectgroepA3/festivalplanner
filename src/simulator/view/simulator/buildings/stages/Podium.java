package simulator.view.simulator.buildings.stages;

import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;
import simulator.view.simulator.visitors.VisitorCalculator;
import models.*;

public class Podium extends TekenObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3248078320645569998L;
	static String infoAlfa;
	static String infoBeta;
	private Stage stage;
	private static final String images[] = {"images/buildings/mainstage.png","images/buildings/betastage.png","images/buildings/charliestage.png"};

	public Podium(Stage stage, int num) {
		super(images[num % images.length],"stage",stage.getName(),null);
		this.stage = stage;
	}

	public Podium(Point2D position) {
		super(images[0], position);
	}
	public static String getInfoAlfa() {
		return infoAlfa;
	}


	public static String getInfoBeta() {
		return infoBeta;
	}

	
	public void setStage(Stage s) {
		this.stage = s;
	}
	
	public Stage getStage() {
		return stage;
	}

	public Event getCurrentEvent() {
		double time = this.simulator.getTimer().getTime();
		return VisitorCalculator.getCurrentEvent(stage, (int) time);
	}
}
