package simulator.view.simulator.buildings.utility;
import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class EHBO extends TekenObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4642666647853296313L;
	public EHBO() {
		super("images/buildings/ehbostand.png","util","EHBO", null);
		this.popularity = 1;
	}
	public EHBO(Point2D position) {
		super("images/buildings/ehbostand.png", position);
		this.popularity = 1;
	}

}
