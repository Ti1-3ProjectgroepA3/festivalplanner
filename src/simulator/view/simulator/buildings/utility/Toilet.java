package simulator.view.simulator.buildings.utility;
import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class Toilet extends TekenObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3291208070769927095L;
	public Toilet() {
		super("images/buildings/toilet.png","util","Toiletten",null);
		this.popularity = 10;
	}
	public Toilet(Point2D position) {
		super("images/buildings/toilet.png", position);
		this.popularity = 10;
	}

}
