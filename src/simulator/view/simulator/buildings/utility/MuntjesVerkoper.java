package simulator.view.simulator.buildings.utility;
import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class MuntjesVerkoper extends TekenObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3151122417578105820L;
	public MuntjesVerkoper(){
		super("images/buildings/coinstand.png","util","Muntjesverkoop",null);
		this.popularity = 10;
	}
	public MuntjesVerkoper(Point2D position) {
		super("images/buildings/coinstand.png", position);
		this.popularity = 10;
	}

}
