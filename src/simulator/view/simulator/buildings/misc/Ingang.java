package simulator.view.simulator.buildings.misc;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import simulator.view.simulator.buildings.TekenObject;

public class Ingang extends TekenObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -226086709941350247L;

	public Ingang() {
		super("images/buildings/entrance.png", "misc", "Ingang", null);
	}

	public Ingang(Point2D position) {
		super("images/buildings/entrance.png", position);
	}

	@Override
	public void mouseDragged(MouseEvent e, Point2D clickPoint,
			Point2D lastClickPosition) {
		// TODO Auto-generated method stub
		// super.mouseDragged(e, clickPoint, lastClickPosition);

		this.position = clickPoint;
		mouseReleased(e, clickPoint);
	}

	public void mouseReleased(MouseEvent e, Point2D clickPoint) {
		// TODO Auto-generated method stub
		List<Point2D> points = new ArrayList<>();

		points.add(new Point2D.Float(simulator.getScreenWidth() / 2, 0)); // N
		points.add(new Point2D.Float(simulator.getScreenWidth(), simulator
				.getScreenHeight() / 2)); // O
		points.add(new Point2D.Float(simulator.getScreenWidth() / 2, simulator
				.getScreenHeight())); // Z
		points.add(new Point2D.Float(0, simulator.getScreenHeight() / 2)); // W
		Point2D closedPoint = null;
		double closedPointDistence = 0;
		for (Point2D p : points) {
			Point2D p2 = new Point2D.Double(this.position.getX(), // +
																	// dragObject.getWidth()
																	// /
																	// 2,
					this.position.getY() // +
											// dragObject.getHeight()
											// / 2
			);
			if (closedPoint == null) {
				closedPoint = p;
				closedPointDistence = p2.distance(closedPoint) - p2.distance(p);
			}
			if (p2.distance(closedPoint) - p2.distance(p) > closedPointDistence) {

				closedPoint = p;
				closedPointDistence = p2.distance(closedPoint) - p2.distance(p);
			}
		}
		double posX = this.position.getX();
		double posY = this.position.getY();

		switch (points.indexOf(closedPoint)) {
		case 0: // N

			if (posX > simulator.getScreenWidth() - getWidth())
				posX = simulator.getScreenWidth() - getWidth();
			if (posX < 0)
				posX = 0;
			System.out.println(getWidth());
			System.out.println(posX);

			this.position = new Point2D.Double(posX, 0);
			this.rotation = 0;
			break;
		case 1: // O
			if (this.position.getY() > simulator.getScreenHeight()
					- getHeight() * 2)
				posY = simulator.getScreenHeight() - (getHeight() * 2);
			if (this.position.getY() < getHeight())
				posY = getHeight();
			this.position = new Point2D.Double(simulator.getScreenWidth()
					- (this.getHeight() * 2), posY);
			this.rotation = 90;
			break;
		case 2: // Z
			if (posX > simulator.getScreenWidth() - getWidth())
				posX = simulator.getScreenWidth() - getWidth();
			if (posX < 0)
				posX = 0;

			this.position = new Point2D.Double(posX,
					simulator.getScreenHeight() - (this.getHeight()));
			this.rotation = 0;
			break;
		case 3: // W
			if (this.position.getY() > simulator.getScreenHeight()
					- getHeight() * 2)
				posY = simulator.getScreenHeight() - (getHeight() * 2);
			if (this.position.getY() < getHeight())
				posY = getHeight();

			this.position = new Point2D.Double((this.getHeight() * -1), posY);

			this.rotation = 90;
			break;

		default:
			break;
		}

	}
}
