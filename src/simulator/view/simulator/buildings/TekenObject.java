package simulator.view.simulator.buildings;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import simulator.view.sidebar.properties.Con_Properties;
import simulator.view.simulator.ConSimulator;
import simulator.view.simulator.DrawObject;

public class TekenObject extends DrawObject implements Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5837423273627408847L;

	protected transient Con_Properties prop;

	public Point2D position;
	public double rotation;
	public double scale;
	public transient Image image;
	protected transient ConSimulator simulator;
	private String categorie;
	private String naam;
	private String filename;
	private int borderColor = 0xff00;
	private String info;

	private Shape testShape;
	protected int popularity = 20;

	// 2d tekenen
	public TekenObject(String filename, Point2D position) {
		this(filename, null, null, position);
	}

	// listitem
	public TekenObject(String filename, String categorie, String naam,
			Point2D position) {
		image = new ImageIcon(filename).getImage();
		scale = 1;
		rotation = 0;
		this.naam = naam;
		this.filename = filename;
		this.categorie = categorie;
		this.position = position;
	}

	// cloneconstructor
	public TekenObject(TekenObject object) {
		this(object.getFileName(), object.getCategorie(), object.getNaam(),
				object.getPosition());
		this.simulator = object.simulator;
		this.prop = object.prop;
	}

	public void loadImage() {
		image = new ImageIcon(filename).getImage();
		System.out.println("Load image: " + filename);
	}

	public void setConProperties(Con_Properties prop) {
		this.prop = prop;
	}

	public void setSimulator(ConSimulator simulator) {
		this.simulator = simulator;
	}

	public void draw(Graphics2D g) {
		AffineTransform tx = getTransform();
		g.drawImage(image, tx, null);
		g.setColor(new Color(borderColor));
		Area a = new Area(new Rectangle2D.Double(0, 0,
				(int) image.getWidth(null), (int) image.getHeight(null)));
		g.draw(a.createTransformedArea(tx));

		g.setColor(Color.PINK);
		if (testShape != null)
			g.draw(testShape);

	}

	private AffineTransform getTransform() {
		AffineTransform tx = new AffineTransform();
		tx.translate(position.getX(), position.getY());

		tx.scale(scale, scale);
		tx.rotate(Math.toRadians(rotation), image.getWidth(null) / 2,
				image.getHeight(null) / 2);
		return tx;
	}

	public boolean contains(Point2D point) {
		Shape shape = new Rectangle2D.Double(0, 0, image.getWidth(null),
				image.getHeight(null));
		return getTransform().createTransformedShape(shape).contains(point);
	}

	public boolean checkCollsion(TekenObject obj) {
		AffineTransform tx = getTransform();

		Area a = new Area(new Rectangle2D.Double(0, 0,
				(int) image.getWidth(null), (int) image.getHeight(null)));
		a.transform(getTransform());

		Area a2 = new Area(
				new Rectangle2D.Double(0, 0, (int) obj.image.getWidth(null),
						(int) obj.image.getHeight(null)));
		a2.transform(obj.getTransform());

		a.intersect(a2);
		return !a.isEmpty();
	}

	public boolean checkCollsion(Line2D s2) {
		Rectangle2D r1 = new Rectangle2D.Double(position.getX(),
				position.getY(), (int) image.getWidth(null) * scale,
				(int) image.getHeight(null) * scale);
		return r1.intersectsLine(s2);
	}

	public Point2D getPosition() {
		return position;
	}

	public double getRotation() {
		return rotation;
	}

	public double getScale() {
		return scale;
	}

	public Image getImage() {
		return image;
	}

	public String getCategorie() {
		return categorie;
	}

	public String getNaam() {
		return naam;
	}

	public String getFileName() {
		return filename;
	}

	public double getWidth() {
		return image.getWidth(null) * scale;
	}

	public double getHeight() {
		return image.getHeight(null) * scale;
	}

	public double getWidthNoScale() {
		return image.getWidth(null);
	}

	public double getHeightNoScale() {
		return image.getHeight(null);
	}

	public void setColor(int i) {
		// TODO Auto-generated method stub
		this.borderColor = i;
	}

	public void mouseReleased(MouseEvent e, Point2D clickPoint) {
		TekenObject collionObject = null;
		for (TekenObject o : simulator.getSimulatorController()
				.getSimulatorObjects()) {
			if (o != this && o.checkCollsion(this)) {
				collionObject = o;
				break;
			}
		}
		if (collionObject == null)
			return;

		do {
			this.position = new Point2D.Double(
					(double) (this.position.getX() + collionObject.getWidth() - (this.position
							.getX() - collionObject.position.getX())),
					(double) (this.position.getY() + collionObject.getHeight() - (this.position
							.getY() - collionObject.position.getY())));

			collionObject = null;
			for (TekenObject o : simulator.getSimulatorController()
					.getSimulatorObjects()) {
				if (o != this && o.checkCollsion(this)) {
					collionObject = o;
					break;
				}
			}
		} while (collionObject != null);
	}

	public void mouseDragged(MouseEvent e, Point2D clickPoint,
			Point2D lastClickPosition) {
		if (SwingUtilities.isLeftMouseButton(e)) {

			this.position = new Point2D.Double(this.position.getX()
					- (lastClickPosition.getX() - clickPoint.getX()),
					this.position.getY()
							- (lastClickPosition.getY() - clickPoint.getY()));
			TekenObject collionObject = null;
			for (TekenObject o : simulator.getSimulatorController()
					.getSimulatorObjects()) {
				if (o != this && o.checkCollsion(this)) {
					collionObject = o;
					break;
				}
			}
			if (collionObject == null)
				this.setColor(0xff00);
			else
				this.setColor(0xff0000);

		} else {
			this.rotation += (lastClickPosition.getX() - clickPoint.getX()) / 3;
		}
	}

	public void mouseMoved(MouseEvent e, Point2D clickPoint,
			Point2D lastClickPosition) {

		this.position = new Point2D.Double(
				(e.getX() / simulator.getCameraScale()),
				(e.getY() / simulator.getCameraScale()));
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public int getPopularty() {
		return popularity;
	}
}
