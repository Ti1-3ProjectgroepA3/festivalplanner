package simulator.view.simulator.buildings.area;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import simulator.view.simulator.DrawObject;
import simulator.view.simulator.buildings.TekenObject;

public class AreaWaypoint extends DrawObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7897636008818095007L;
	TekenObject tekenObject;
	Point2D position;

	int tekenObjectPostion;
	double height = 100;
	double weigth = 100;

	public AreaWaypoint(TekenObject tekenObject) {
		this.tekenObject = tekenObject;
	}

	public AreaWaypoint(Point2D point2D) {
		this.position = point2D;
	}

	private AffineTransform getTransformation() {
		AffineTransform tx = new AffineTransform();
		if (tekenObject != null) {
			tx.translate(tekenObject.position.getX(),
					tekenObject.position.getY());
			weigth = tekenObject.getWidthNoScale();
			tx.scale(tekenObject.scale, tekenObject.scale);
			tx.rotate(Math.toRadians(tekenObject.getRotation()),
					tekenObject.getWidthNoScale() / 2,
					tekenObject.getHeightNoScale() / 2);
		} else {
			tx.translate(position.getX(), position.getY());

		}
		return tx;
	}

	public void draw(Graphics2D g2) {
		Area a;
		if (tekenObject != null) {
			a = new Area(new Rectangle2D.Double(0,
					tekenObject.getHeightNoScale(), weigth, height));
		} else {
			a = new Area(new Rectangle2D.Double(0, 0, weigth, height));

		}
		g2.setColor(new Color(.02f, .02f, .02f, 0.2f));
		g2.fill(a.createTransformedArea(getTransformation()));
		g2.setColor(Color.cyan);
		Point2D p = getCenterPoint();
		g2.fill(new Rectangle2D.Double(p.getX(), p.getY(), 5, 5));
	}

	public boolean contains(Point2D point) {
		Shape shape;
		if (tekenObject != null) {
			shape = new Rectangle2D.Double(0,
					tekenObject.getHeightNoScale(), weigth, height);
		} else {
			shape = new Rectangle2D.Double(0, 0, weigth, height);

		}
		return getTransformation().createTransformedShape(shape).contains(point);
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public void setWeight(double weight) {
		this.weigth = weight;
	}

	public boolean attachedToTekenObject() {
		return tekenObject != null;
	}

	public TekenObject getTekenObject() {
		return tekenObject;
	}

	public double getWeigth() {
		return weigth;
	}

	public double getHeight() {
		return height;
	}

	public Point2D getCenterPoint() {
		if (attachedToTekenObject()) {

			return getTransformation().transform(
					new Point2D.Double(weigth / 2,
							tekenObject.getHeightNoScale() + height / 2), null);
		} else
			return new Point2D.Double(position.getX() + weigth / 2,
					position.getY() + height / 2);
	}

}
