package simulator.view.simulator.buildings.food;


import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class Coffee extends TekenObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9169391910450669569L;

	public Coffee(){
		super("images/buildings/coffeestand.png","food","Coffee stand",null);
		this.popularity = 40;
	}
	
	public Coffee(Point2D position) {
		super("images/buildings/coffeestand.png", position);
		this.popularity = 40;
	}

}
