package simulator.view.simulator.buildings.food;
import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class Burger extends TekenObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7249681045076874391L;

	public Burger(){
		super("images/buildings/burgerstand.png","food","Burger stand",null);
		this.popularity = 20;
	}
	
	public Burger(Point2D position) {
		super("images/buildings/burgerstand.png", position);
		this.popularity = 20;
	}

}
