package simulator.view.simulator.buildings.food;
import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class Noodles extends TekenObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2308113460381133593L;

	public Noodles(){
		super("images/buildings/noodlestand.png","food","Noodle stand",null);
	}
	
	public Noodles(Point2D position) {
		super("images/buildings/noodlestand.png", position);
	}

}
