package simulator.view.simulator.buildings.food;
import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class Ice extends TekenObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8975873451024732805L;

	public Ice(){
		super("images/buildings/icestand.png","food","Ice cream stand",null);
		this.popularity = 30;
	}
	
	public Ice(Point2D position) {
		super("images/buildings/icestand.png", position);
		this.popularity = 30;
	}

}
