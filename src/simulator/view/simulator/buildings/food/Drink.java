package simulator.view.simulator.buildings.food;


import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class Drink extends TekenObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8619459647195740827L;

	public Drink(){
		super("images/buildings/drinkstand.png","food","Soda and Beer stand",null);
		this.popularity = 40;
	}
	
	public Drink(Point2D position) {
		super("images/buildings/drinkstand.png", position);
		this.popularity = 40;
	}

}
