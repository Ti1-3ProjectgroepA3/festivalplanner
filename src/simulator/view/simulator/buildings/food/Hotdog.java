package simulator.view.simulator.buildings.food;
import java.awt.geom.Point2D;

import simulator.view.simulator.buildings.TekenObject;


public class Hotdog extends TekenObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 137876386125339509L;

	public Hotdog(){
		super("images/buildings/hotdogstand.png","food","Hotdog stand",null);
		this.popularity = 20;
	}
	
	public Hotdog(Point2D position) {
		super("images/buildings/hotdogstand.png", position);
		this.popularity = 20;
	}

}
