package simulator.view.simulator;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import simulator.view.sidebar.properties.Con_Properties;
import simulator.view.simulator.buildings.TekenObject;
import simulator.view.simulator.buildings.area.AreaWaypoint;
import simulator.view.simulator.buildings.misc.Ingang;
import simulator.view.simulator.events.SimulatorMouseListener;
import simulator.view.simulator.timer.TimerThread;
import simulator.view.simulator.visitors.Visitor;
import simulator.view.simulator.visitors.VisitorCalculator;
import controllers.SimulatorController;

public class ConSimulator extends JPanel {
	private VisitorCalculator visitorCalculator;
	public Thread spawn;
	private BufferedImage background;
	private Con_Properties props;
	private SimulatorController simulatorController;
	private ArrayList<Point2D> entranceLocs = new ArrayList<Point2D>();

	private Random random = new Random();
	private Point2D cameraPoint;
	public static boolean running = false;
	private double cameraScale = 1;

	private int screenWidth = 1000;
	private int screenHeight = 1000;
	private int count;
	private boolean areaBuildMode = false;
	private boolean deleteMode = false;
	private boolean reachedMaxAmount = false;

	Thread updateGraphics;
	private TimerThread timer;

	public ConSimulator() {

		this(1000, 1000);
	}

	public ConSimulator(int screenWidth, int screenHeight) {

		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;

		simulatorController = new SimulatorController(this, props);
		props = new Con_Properties(this);
		cameraPoint = new Point2D.Double(screenWidth/ 2, screenHeight / 2);
		visitorCalculator = new VisitorCalculator(this, simulatorController);
		try {
			background = ImageIO.read(new File("images/grass.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		SimulatorMouseListener listener = new SimulatorMouseListener(this,
				simulatorController);
		addMouseListener(listener);
		addMouseMotionListener(listener);
		addMouseWheelListener(listener);

		updateGraphics = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while (true) {
					try {
						repaint();
						Thread.sleep(1000 / 60);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		updateGraphics.start();

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		AffineTransform oldTransform = g2.getTransform();
		g2.setTransform(getCamera());

		TexturePaint p = new TexturePaint(background, new Rectangle2D.Double(0,
				0, 100, 100));
		g2.setPaint(p);
		g2.fill(new Rectangle2D.Double(0, 0, screenWidth, screenHeight));
		g2.setColor(Color.red);
		try {
			for (DrawObject d : simulatorController.getDrawObjects()) {
				d.draw(g2);
			}

		} catch (Exception e) {
			// e.printStackTrace();
		}
		g2.setClip(null);
		g2.setTransform(oldTransform);
	}

	private AffineTransform getCamera() {
		AffineTransform tx = new AffineTransform();
		tx.translate(-cameraPoint.getX() + getWidth() / 2, -cameraPoint.getY()
				+ getHeight() / 2);
		tx.scale(cameraScale, cameraScale);

		return tx;
	}

	public Point2D getClickPoint(Point point) {
		try {
			return getCamera().inverseTransform(point, null);
		} catch (NoninvertibleTransformException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	public void spawnVisitors(int amount) {

		if (spawn != null && spawn.isAlive()) {
			spawn.interrupt();

		}
		spawn = new Thread(new Runnable() {
			public void run() {
				try {
					if (amount <= count) {
						reachedMaxAmount = true;

						if (amount < count) {
							int removeAmount = count - amount;
							for (int i = 0; removeAmount > i; i++) {
								simulatorController.getVisitors().remove(
										simulatorController.getVisitors()
												.size() - 1);
							}
							count = count - removeAmount;
							System.out.println("Visitors removed: "
									+ removeAmount);
						}
					}
					if (amount > count) {
						reachedMaxAmount = false;
					}
					
					for (AreaWaypoint a : simulatorController
							.getAreas()) {
						if (a.getTekenObject() instanceof Ingang) {
							entranceLocs.add(a.getCenterPoint());
						}
					}
					if (entranceLocs == null)
						return;
					if (!reachedMaxAmount) {
						for (int i = (0 + count); i < amount; i++) {
							Visitor v = new Visitor(simulatorController, entranceLocs.get(random.nextInt(entranceLocs.size())));
							simulatorController.getVisitors().add(v);
							v.setTarget(visitorCalculator.getRandomTarget());
							count++;
							Thread.sleep(500);
						}
					}
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		spawn.start();
	}

	public void pauseSpawn() {
		try {
			if (spawn.isAlive()) {
				spawn.interrupt();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void clearStopSpawn(){
		try {
			if (spawn.isAlive()) {
				spawn.interrupt();
			}
			simulatorController.getVisitors().clear();
			count = 0;
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public double getCameraScale() {
		return cameraScale;
	}

	public void setCameraScale(double cameraScale) {
		this.cameraScale = cameraScale;
		repaint();
	}

	public Point2D getCameraPoint() {
		return cameraPoint;
	}

	public void setCameraPoint(Point2D cameraPoint) {
		this.cameraPoint = cameraPoint;
	}

	public Con_Properties getConProperties() {
		return props;
	}

	public SimulatorController getSimulatorController() {
		return simulatorController;
	}

	public void toggleAreaMode() {
		if(!areaBuildMode){
			areaBuildMode = true;
		}else{
			areaBuildMode = false;
		}
		setDeleteMode(false);
	}

	public void setAreaMode(boolean mode) {
		areaBuildMode = mode;
	}

	public void toggleDeleteMode() {
		if(!deleteMode){
			deleteMode = true;
		}else{
			deleteMode = false;
		}
		setAreaMode(false);
	}

	public void setDeleteMode(boolean mode) {
		deleteMode = mode;
	}

	public boolean getAreaMode() {
		return areaBuildMode;
	}

	public boolean getDeleteMode() {
		return deleteMode;
	}

	public int getScreenWidth() {
		return screenWidth;
	}

	public int getScreenHeight() {
		return screenHeight;
	}

	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}

	public void setScreenHeight(int screenHeight) {
		this.screenHeight = screenHeight;
	}

	public void add(DrawObject o) {
		simulatorController.add(o);
	}

	public VisitorCalculator getVisitorCalculator() {
		return visitorCalculator;
	}
	
	public TimerThread getTimer() {
		return timer;
	}

	public void setTimer(TimerThread timer) {
		// TODO Auto-generated method stub
		this.timer = timer;
	}

}
