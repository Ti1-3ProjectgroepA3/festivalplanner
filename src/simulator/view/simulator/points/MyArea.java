package simulator.view.simulator.points;

import java.awt.Rectangle;

public class MyArea {
	
	private Rectangle rectangle;
	
	public MyArea(Rectangle rectangle){
		this.rectangle = rectangle;
	}
	public Rectangle getRectangle(){
		return rectangle;
	}
}
