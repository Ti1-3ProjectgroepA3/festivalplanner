package simulator.view.simulator;

import java.util.Iterator;

import simulator.view.simulator.visitors.Visitor;
import controllers.SimulatorController;

public class TimerThread extends Thread {
	ConSimulator sim;
	SimulatorController simulatorController;

	double speed = 1;

	double time = (60 * 60 * 8);
	
	public TimerThread(ConSimulator sim, SimulatorController simulatorController) {
		this.sim = sim;
		this.simulatorController = simulatorController;
	}
	
	

	public void run() {
		while (true) {
			
			try {
				Iterator<Visitor> it = simulatorController.getVisitors().iterator();
				while(it.hasNext()) {
					Visitor v = it.next();
					if (ConSimulator.running == true){
//						v.update(simulatorController.getVisitors(),speed);
					}
				}
				sim.repaint();

				Thread.sleep(1000/60);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}



	public double getSpeed() {
		return speed;
	}



	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	
}
