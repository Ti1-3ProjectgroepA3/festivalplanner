package simulator.menubar;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import simulator.view.simulator.ConSimulator;
import controllers.SimulatorController;
import main.AppSelector;
import main.Main;

public class MenuActionListener implements ActionListener {
	CardLayout cardLayout;
	JPanel c;
	String currentFile;
	Main main;

	ConSimulator conSimulator;
	SimulatorController simulatorController;

	public MenuActionListener(ConSimulator conSimulator,
			SimulatorController simulatorController) {
		this.conSimulator = conSimulator;
		this.simulatorController = simulatorController;
	}

	public void actionPerformed(ActionEvent e) {

		switch (e.getActionCommand()) {
		case "New":
			// todo
			break;
		case "Save":
			if (currentFile == null || currentFile == "") {
				currentFile = getFileLoc(true);
				if (currentFile != null)
					simulatorController.exportSimulator(currentFile);
			}

			JOptionPane.showMessageDialog(null, "Festival saved.");
			break;
		case "Save as":
			currentFile = AppSelector.getFileLoc(true, "simulator");
			if (currentFile != null)
				simulatorController.exportSimulator(currentFile);
			// FestivalController.instance.saveToFile(currentFile);
			JOptionPane.showMessageDialog(null, "Festival saved.");
			break;
		case "Load":

			currentFile = getFileLoc(false);
			// FestivalController.instance.loadToFile(currentFile);
			// main.loadLayout();
			break;
		case "Exit":
			System.exit(0);
			break;
		case "Lineup":
			cardLayout.show(c, "LineUp");
			break;
		case "Agenda":
			cardLayout.show(c, "TableView");
			break;
		case "Editor":
			cardLayout.show(c, "FestivalEditor");
			break;
		}

	}

	public String getFileLoc(boolean save) {
		JFileChooser chooser = new JFileChooser();
		FileFilter filter = new FileNameExtensionFilter("simulator", "simulator");
		chooser.addChoosableFileFilter(filter);
		chooser.setAcceptAllFileFilterUsed(false);
		if (save) {
			chooser.setFileSelectionMode(JFileChooser.SAVE_DIALOG);
		} else {
			chooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
		}
		int option = chooser.showOpenDialog(null);
		if (option == JFileChooser.APPROVE_OPTION) {
			File sf = chooser.getSelectedFile();
			String path = sf.getPath();
			if (!path.endsWith(".simulator"))
				return path + ".simulator";
			return path;
		}

		return "";

	}
}
