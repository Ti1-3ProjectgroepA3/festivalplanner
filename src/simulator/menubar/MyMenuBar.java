package simulator.menubar;

import java.awt.CardLayout;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import simulator.view.simulator.ConSimulator;
import controllers.SimulatorController;
import main.Main;

public class MyMenuBar extends JMenuBar{
	public MyMenuBar(ConSimulator conSimulator, SimulatorController simulatorController){
		
		//initialize menu components
		JMenu fileMenu = new JMenu("File");
		//file menu components
		JMenuItem newItem = new JMenuItem("New",KeyEvent.VK_R);
//		fileMenu.add(newItem);
		
		JMenuItem newMenuItem = new JMenuItem("Save", KeyEvent.VK_N);
		fileMenu.add(newMenuItem);

		JMenuItem saveItem = new JMenuItem("Save as", KeyEvent.VK_S);
		fileMenu.add(saveItem);

		JMenuItem loadItem = new JMenuItem("Load",KeyEvent.VK_L);
//		fileMenu.add(loadItem);

		JMenuItem exitItem = new JMenuItem("Exit",KeyEvent.VK_F4);
		fileMenu.add(exitItem);

		//set shortcuts
		fileMenu.setMnemonic(KeyEvent.VK_F);
	
		MenuActionListener menuActionListener = new MenuActionListener(conSimulator, simulatorController);
		
		//actionlisteners
		newMenuItem.addActionListener(menuActionListener);
		newItem.addActionListener(menuActionListener);
		saveItem.addActionListener(menuActionListener);
		loadItem.addActionListener(menuActionListener);
		exitItem.addActionListener(menuActionListener);
		
		//add components to MenuBar
		this.add(fileMenu);

	}
}
