package main;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import screens.editor.FestivalEditor;
import screens.lineup.LineUp;
import screens.menubar.MyMenuBar;
import screens.planner.PlannerView;
import screens.tableview.TableView;
import simulator.view.simulator.ConSimulator;
import controllers.FestivalController;

public class AppSelector extends JFrame {
	JButton button_planner = new JButton("Start simulator");
	JButton button_agenda = new JButton("Start agendamodule");
	JButton button_loadAgenda = new JButton("Laad agenda in");
	JButton button_loadFestival = new JButton("Laad terrein in");
	JFrame mainFrame;
	FestivalController controller = FestivalController.instance;
	String loadFileSimulator;

	public AppSelector() {
		setTitle("Festival planner");
		this.setLayout(new GridLayout(1, 2));

		this.add(paddingBox("Agenda settings", getAgendaPanel()));
		this.add(paddingBox("Simulator settings", getSimulatorPanel()));

		this.setPreferredSize(new Dimension(800, 600));
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		button_agenda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AppSelector.this.setVisible(false);
				showAgenda();
			}
		});
		button_planner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AppSelector.this.setVisible(false);
				showSimulator();
			}
		});
		button_loadFestival.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadFileSimulator = AppSelector.getFileLoc(false, "simulator");
			}
		});
	}

	private JPanel paddingBox(String title, JPanel box) {
		JPanel panel = new JPanel();
		JLabel l = new JLabel(title);
		panel.add(l);
		panel.add(Box.createHorizontalStrut(5));
		panel.add(box);
		panel.setBorder(BorderFactory.createLineBorder(Color.gray));
		return panel;
	}

	private JPanel getAgendaPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(button_agenda);
		panel.add(button_loadAgenda);
		button_loadAgenda.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String file = getFileLoc(false, "festival");
				if (file == null) {
					// Geen file
				} else {
					FestivalController.instance.loadToFile(file);
				}

			}
		});
		return panel;
	}

	JSpinner width;
	JSpinner height;

	private JPanel getSimulatorPanel() {
		JPanel settings = new JPanel(new GridLayout(2, 2));
		width = new JSpinner(new SpinnerNumberModel(1200, 500, 3000, 50));
		height = new JSpinner(new SpinnerNumberModel(1200, 500, 3000, 50));
		settings.add(new JLabel("Simulator width: "));
		settings.add(width);
		settings.add(new JLabel("Simulator height: "));
		settings.add(height);
		JPanel buttonpanel = new JPanel(new GridLayout(2, 1));
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(settings, BorderLayout.CENTER);
		buttonpanel.add(button_planner);
		buttonpanel.add(button_loadFestival);
		panel.add(buttonpanel, BorderLayout.SOUTH);
		return panel;
	}

	public void reloadWindow() {

	}

	private void showSimulator() {
		simulator.ContentPane contentPane = new simulator.ContentPane();
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setContentPane(contentPane);
		mainFrame.setJMenuBar(new simulator.menubar.MyMenuBar(contentPane.getSim(), contentPane.getSim().getSimulatorController()));
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		mainFrame.setVisible(true);
		mainFrame.addWindowListener(new WindowAdapter() {

			public void windowClosed(WindowEvent e) {
				setVisible(true);
			}
		});

		contentPane.getSim().setScreenHeight((int) height.getValue());
		contentPane.getSim().setScreenWidth((int) width.getValue());
		if (loadFileSimulator != null)
			contentPane.getSim().getSimulatorController()
					.importSimulator(loadFileSimulator);

	}

	FestivalEditor editor;

	private void showAgenda() {

		CardLayout cardLayout = new CardLayout();
		JPanel cards = new JPanel(cardLayout);
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setPreferredSize(new Dimension(800, 600));
		mainFrame.setJMenuBar(new MyMenuBar(cardLayout, cards, mainFrame));
		JPanel tableView = new TableView();
		JPanel lineUp = new LineUp(FestivalController.instance);
		editor = new FestivalEditor();
		JPanel planner = new PlannerView(this);
		JPanel mainpanel = new JPanel();
		mainpanel.setLayout(new BorderLayout());

		JScrollPane sp = new JScrollPane(planner);
		sp.getVerticalScrollBar().setUnitIncrement(20);
		cards.add(sp, "PlannerView");
		cards.add(lineUp, "LineUp"); // first card
		cards.add(tableView, "TableView");
		cards.add(editor, "FestivalEditor");

		mainpanel.add(cards, BorderLayout.CENTER);
		mainFrame.setContentPane(mainpanel);
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosed(WindowEvent e) {
				setVisible(true);
			}
		});

	}

	public FestivalEditor getEditor() {
		return editor;
	}

	public static String getFileLoc(boolean save, String type) {
		JFileChooser chooser = new JFileChooser();
		FileFilter filter = new FileNameExtensionFilter(type, type);
		chooser.addChoosableFileFilter(filter);
		chooser.setAcceptAllFileFilterUsed(false);
		if (save) {
			chooser.setFileSelectionMode(JFileChooser.SAVE_DIALOG);
		} else {
			chooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
		}
		int option = chooser.showOpenDialog(null);
		if (option == JFileChooser.APPROVE_OPTION) {
			File sf = chooser.getSelectedFile();
			String path = sf.getPath();
			if (!path.endsWith("." + type))
				return path + "." + type;
			return path;
		}

		return null;

	}

}
