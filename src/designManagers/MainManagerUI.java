package designManagers;

import java.awt.Color;

import javax.swing.UIManager;

@SuppressWarnings("serial")
public class MainManagerUI extends UIManager{

	public MainManagerUI(){
		super();
	}
	
	public static void setMyLookAndFeel(){
		put("ScrollBarUI", "designManagers.customComponents.MyScrollbarUI");
	}
}
