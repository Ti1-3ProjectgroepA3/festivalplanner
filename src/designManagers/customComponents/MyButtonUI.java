package designManagers.customComponents;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicButtonUI;

public class MyButtonUI extends BasicButtonUI{
	public MyButtonUI() {
		super();
	}
		
	public static MyButtonUI createUI(JComponent c) {
	       return new MyButtonUI();
	}
	
	public void paint(Graphics g, JComponent c){
		super.paint(g, c);
		c.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		c.setBackground(new Color(46,47,51));
	}
	
	@Override
	protected void paintFocus(Graphics g, AbstractButton b, Rectangle viewRect, Rectangle textRect, Rectangle iconRect){
		super.paintFocus(g, b, viewRect, textRect, iconRect);
		b.setForeground(new Color(240,240,240));
		
	}
	
	@Override
	protected void paintButtonPressed(Graphics g, AbstractButton b){
		super.paintButtonPressed(g, b);
		g.setColor(new Color(148,216,0));
		g.fillRect(b.getX(), b.getY()+4, 3, b.getHeight()-8);
	}
	
	@Override
	protected void paintText(Graphics g, AbstractButton b, Rectangle textRect, String text){
		super.paintText(g, b, textRect, text);
		b.setForeground(new Color(200,200,200));
	}
	
}