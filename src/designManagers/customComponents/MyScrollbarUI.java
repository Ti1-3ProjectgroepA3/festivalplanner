package designManagers.customComponents;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class MyScrollbarUI extends BasicScrollBarUI{
	public MyScrollbarUI() {
		super();
	}
	
	public static BasicScrollBarUI createUI(JComponent c) {
        return new MyScrollbarUI();
    }
	
	@Override
	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds){
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(new Color(28,28,31));
		c.setPreferredSize(new Dimension(11,trackBounds.height));
		g2d.fill(trackBounds);
	}
	@Override
	protected void paintThumb(Graphics g, JComponent c, Rectangle trackThumb){
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(new Color(50,52,56));
		g2d.fillRoundRect(trackThumb.x+1, trackThumb.y+1, trackThumb.width-2, trackThumb.height-1, 10, 10);
	}
	
	 @Override
     protected JButton createDecreaseButton(int orientation) {
         return createZeroButton();
     }
	 
     @Override    
     protected JButton createIncreaseButton(int orientation) {
         return createZeroButton();
     }

     private JButton createZeroButton() {
         JButton jbutton = new JButton();
         jbutton.setPreferredSize(new Dimension(0, 0));
         jbutton.setMinimumSize(new Dimension(0, 0));
         jbutton.setMaximumSize(new Dimension(0, 0));
         return jbutton;
     }
}
