package designManagers.customComponents;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.plaf.basic.BasicLabelUI;

public class MyLabelUI extends BasicLabelUI{
	public MyLabelUI() {
		super();
	}
		
	public static MyLabelUI createUI(JComponent c) {
	       return new MyLabelUI();
	}
	
	@Override
	protected void paintEnabledText(JLabel l, Graphics g, String s, int textX, int textY){
		super.paintEnabledText ( l, g, s, textX, textY );
		l.setForeground(new Color(200,200,200));
	}
	
	@Override
	protected void paintDisabledText(JLabel l, Graphics g, String s, int textX, int textY){
		super.paintDisabledText ( l, g, s, textX, textY );
		l.setForeground(new Color(120,120,240));
	}
	
}