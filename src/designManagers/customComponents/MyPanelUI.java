package designManagers.customComponents;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicPanelUI;

public class MyPanelUI extends BasicPanelUI{
	public MyPanelUI() {
		super();
	}
		
	public static MyPanelUI createUI(JComponent c) {
	       return new MyPanelUI();
	}
	
	@Override
	public void paint(Graphics g, JComponent c){
		super.paint(g, c);
		if(c.getName() != "main"){
			c.setBackground(new Color(34,35,38));
		}else{
			c.setBackground(new Color(18, 19, 20));
		}
		
	}
	
}
	