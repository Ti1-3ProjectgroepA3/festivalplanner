package models;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class FestivalPlanner implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3292081440162467501L;
	protected ArrayList<Stage> stages;
	protected ArrayList<Artist> artists;
	protected ArrayList<Event> events;
	protected Calendar c;
	protected Calendar c2;
	private transient DateFormat f = new SimpleDateFormat("HH:mm:ss.SSS");

	public FestivalPlanner() {
		stages = new ArrayList<Stage>();
		artists = new ArrayList<Artist>();
		events = new ArrayList<Event>();
		insertTestData();
	}

	public void insertTestData() {
		c = new GregorianCalendar(0, 0, 0, 8, 0, 0);
		c2 = new GregorianCalendar(0, 0, 0, 9, 0, 0);
		artists.add(new Artist("Calvin Harris", "images/spiderman.jpg",
				"Ja Calvin Harris"));
		artists.add(new Artist("Ellie Goulding", "images/spiderman.jpg",
				"Redelijke chick denk ik zo."));
		artists.add(new Artist("Echosmith", "test",
				"Het gaat hier uiteraard vooral om sydney."));
		artists.add(new Artist("Eminem", "test", "Just lose it."));
		stages.add(new Stage("Alfa", 100));
		stages.add(new Stage("Beta", 120));
		events.add(new Event(c, c2, 100, artists.get(1), stages.get(0)));

	}

	public boolean checkEventExistWithTime(Event event, Calendar c1, Calendar c2) {
		if (f == null)
			f = new SimpleDateFormat("HH:mm:ss.SSS");

		Date start1 = c1.getTime();
		Date end1 = c2.getTime();
		boolean overlapFound = false;
		for (Event e : getEvents()) {
			if (e.getStage() != event.getStage() && event == e)
				continue;
			Date start2 = e.getStartTime().getTime();
			Date end2 = e.getEndTime().getTime();
			if (start1.getTime() == start2.getTime()
					&& end1.getTime() == end2.getTime()) {
				overlapFound = true;
				break;
			}
			if (f.format(start1).compareTo(f.format(end2)) > 0
					&& f.format(end1).compareTo(f.format(start2)) < 0) {
				overlapFound = true;
				break;
			}

		}
		return overlapFound;
	}

	public void addStage(Stage stage) {
		stages.add(stage);
	}

	public void addArtist(Artist a) {
		artists.add(a);
	}

	public void addEvent(Event e) {
		events.add(e);
	}

	public void deleteArtist(int index) {
		artists.remove(index);
	}

	public void deleteStage(int index) {
		stages.remove(index);
	}

	public void deleteEvent(int index) {
		events.remove(index);
	}

	public ArrayList<Stage> getStages() {
		return stages;
	}

	public ArrayList<Artist> getArtists() {
		return artists;
	}

	public ArrayList<Event> getEvents() {
		return events;
	}
}
