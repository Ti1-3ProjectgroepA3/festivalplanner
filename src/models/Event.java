package models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("serial")
public class Event implements Serializable {

	private Calendar startTime;
	private Calendar endTime;
	private int popularity;
	private ArrayList<Artist> artists;
	private Stage stage;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	
	public Event(Calendar startTime, Calendar endTime, int popularity,
			ArrayList<Artist> artists, Stage stage) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.popularity = popularity;
		this.artists = artists;
		this.stage = stage;
	}

	public Event(Calendar startTime, Calendar endTime, int popularity,
			ArrayList<Artist> artists) {
		this(startTime, endTime, popularity, artists, null);

	}

	public Event(Calendar startTime, Calendar endTime, int popularity,
			Artist artist, Stage stage) {
		this(startTime, endTime, popularity, new ArrayList<Artist>(), stage);
		artists.add(artist);
	}

	public Event(Calendar startTime, Calendar endTime, int popularity,
			Artist artist) {
		this(startTime, endTime, popularity, new ArrayList<Artist>(), null);
		artists.add(artist);
	}

	public Calendar getStartTime() {
		return startTime;
	}

	public String getStartTimeString(){
		Calendar eStartTime = getStartTime();
		Date date1 =  eStartTime.getTime();
		String startTimeString = dateFormat.format(date1);
		return startTimeString;
	}
	
	public String getEndTimeString(){
		Calendar eEndTime = getEndTime();
		Date date2 = eEndTime.getTime();
		String endTimeString = dateFormat.format(date2);	
		return endTimeString;
	}
	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	public Calendar getEndTime() {
		return endTime;
	}

	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}

	public int getPopularity() {
		return popularity;
	}

	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}

	public ArrayList<Artist> getArtists() {
		return artists;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage selectedItem) {
		this.stage = selectedItem;
	}

	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

		String startTime = dateFormat.format(getStartTime().getTime());
		String endTime = dateFormat.format(getEndTime().getTime());
		return "Start: " + startTime + ", Eind: " + endTime;
	}

}
