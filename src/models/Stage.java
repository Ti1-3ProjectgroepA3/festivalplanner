package models;

import java.io.Serializable;

public class Stage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3957986974111587473L;
	private String name;
	private int maxVisitors;
	
	public Stage(String name, int maxVisitors) {
		this.name = name;
		this.maxVisitors = maxVisitors;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxVisitors() {
		return maxVisitors;
	}

	public void setMaxVisitors(int maxVisitors) {
		this.maxVisitors = maxVisitors;
	}
	
	
	public String toString() {
		return name;
	}
}
