package models;

import java.io.Serializable;

public class Artist implements Serializable {

	private String name;
	private String img;
	private String description;
	
	public Artist(String name, String img, String description) {
		this.name = name;
		this.img = img;
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description){
		this.description = description;
	}
	
	public String toString() {
		return name;
	}

}
